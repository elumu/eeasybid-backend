package co.uk.app.entities.common;

import javax.persistence.*;

/**
 * Created by ${Eclair} on 12/1/2018.
 */
@Entity
@Table(name = "contact")
public class Contact {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String mobileNumber;

    public Contact() {
        super();
    }
    public Contact(final Contact contact) {
        this.setMobileNumber(contact.getMobileNumber());
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
