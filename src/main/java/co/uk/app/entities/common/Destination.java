package co.uk.app.entities.common;

import javax.persistence.*;


/**
 * Created by ${Eclair} on 12/30/2018.
 */
@Entity
@Table(name = "route")
public class Destination {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String region;
    private String country;
    private String city;
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    private RangeDates rangeDates;

    public Destination() {
    }

    public Destination(final Destination destination) {
        this.setCity(destination.getCity());
        this.setCountry(destination.getCountry());
        this.setRegion(destination.getRegion());
        this.setRangeDates(destination.getRangeDates());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }


    public RangeDates getRangeDates() {
        return rangeDates;
    }

    public void setRangeDates(RangeDates rangeDates) {
        this.rangeDates = rangeDates;
    }
}
