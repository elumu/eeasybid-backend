package co.uk.app.entities.common;

import javax.persistence.*;

/**
 * Created by ${Eclair} on 12/30/2018.
 */
@Entity
@Table(name = "range_date")
public class RangeDates {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String fromDate;
    private String toDate;

    public RangeDates() {
    }

    public RangeDates(final RangeDates range) {
       this.setFromDate(range.getFromDate());
       this.setToDate(range.getToDate());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }
}
