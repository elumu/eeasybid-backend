package co.uk.app.entities.common;

import co.uk.app.account.entities.UserAccountProfile;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by ${Eclair} on 12/1/2018.
 */
@Entity
@Table(name = "address")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String address1;
    private String address2;
    private String city;
    private String postcode;
    private String country;
    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            }, mappedBy = "addresses")
    private Set<UserAccountProfile> accountProfiles;

    public Address() {
        super();
    }

    public Address(final Address address) {
        this.setAddress1(address.getAddress1());
        this.setAddress2(address.getAddress2());
        this.setCity(address.getCity());
        this.setPostcode(address.getPostcode());
        this.setCountry(address.getCountry());
        this.setAccountProfiles(address.getAccountProfiles());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<UserAccountProfile> getAccountProfiles() {
        return accountProfiles;
    }

    public void setAccountProfiles(Set<UserAccountProfile> accountProfiles) {
        this.accountProfiles = accountProfiles;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
