package co.uk.app.entities.common;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Created by ${Eclair} on 5/20/2019.
 */
@Entity
@Table(name = "files")
public class File {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
    private String path;
    private String type;
    @Lob
    private byte[] data;
    private String owner;

    public File() {
        super();
    }

    public File(String path, String type, byte[] data, String owner) {
        this.path = path;
        this.type = type;
        this.data = data;
        this.owner = owner;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
