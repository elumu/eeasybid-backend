package co.uk.app.entities.commerce;

import javax.persistence.*;


/**
 * Created by ${Eclair} on 10/21/2018.
 */
@Entity
@Table(name = "order_entry")
public class UserOrderEntry {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id")
    private Order order;

    public UserOrderEntry(final UserOrderEntry userOrderEntry){
        this.setId(userOrderEntry.getId());
        this.setOrder(userOrderEntry.order);
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
