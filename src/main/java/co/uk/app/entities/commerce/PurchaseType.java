package co.uk.app.entities.commerce;

/**
 * Created by ${Eclair} on 10/21/2018.
 */
public enum PurchaseType {
    CURRENT_PURCHASE,
    ARCHIVE_PURCHASE
}
