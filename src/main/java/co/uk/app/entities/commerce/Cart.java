package co.uk.app.entities.commerce;

import co.uk.app.entities.product.Product;
import co.uk.app.utilities.Auditable;

import javax.persistence.*;
import java.util.List;

/**
 * Created by ${Eclair} on 10/21/2018.
 */
@Entity
@Table(name="cart")
public class Cart extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @OneToMany(mappedBy = "cart", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Product> products;
    private Long cartOwner;

    public Cart() {
        super();
    }

    public Cart(final Cart cart){
        this.setId(cart.getId());
        this.setCartOwner(cart.getCartOwner());
        this.setProducts(cart.getProducts());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Long getCartOwner() {
        return cartOwner;
    }

    public void setCartOwner(Long cartOwner) {
        this.cartOwner = cartOwner;
    }
}
