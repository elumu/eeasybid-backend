package co.uk.app.entities.commerce;

import co.uk.app.utilities.Auditable;
import co.uk.app.utilities.OrderStatus;

import javax.persistence.*;


/**
 * Created by ${Eclair} on 10/21/2018.
 */
@Entity
@Table(name = "orders")
public class Order extends Auditable<String> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private OrderStatus orderStatus;
    @OneToOne()
    private Cart shoppingCart;
    private float price;
    public Order() {
        super();
        this.setOrderStatus(OrderStatus.NEW_ORDER);
    }
    public Order(final Order order){
        this.setId(order.getId());
        this.setOrderStatus(order.getOrderStatus());
        this.setShoppingCart(order.getShoppingCart());
        this.setPrice(order.getPrice());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public OrderStatus getOrderStatus() {

        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Cart getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(Cart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }
}
