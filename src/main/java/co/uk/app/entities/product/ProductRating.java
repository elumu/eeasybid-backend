package co.uk.app.entities.product;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;


@Entity
@Table(name = "rating")
public class ProductRating
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public long id;
	public Integer rating;
	@OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, optional = false)
	public ProductReview productReview;

	public ProductRating(final Integer rating, final ProductReview productReview)
	{
		this.rating = rating;
		this.productReview = productReview;
	}

	public long getId()
	{
		return id;
	}

	public void setId(final long id)
	{
		this.id = id;
	}

	public Integer getRating()
	{
		return rating;
	}

	public void setRating(final Integer rating)
	{
		this.rating = rating;
	}

	public ProductReview getProductReview()
	{
		return productReview;
	}

	public void setProductReview(final ProductReview productReview)
	{
		this.productReview = productReview;
	}
}
