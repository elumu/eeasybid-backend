package co.uk.app.entities.product;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;


/**
 * Created by ${Eclair} on 10/23/2018.
 */
@Entity
@Table(name = "review")
public class ProductReview {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String reviewer;
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, optional = false)
    private ProductRating rating;
    @Lob
    private String contents;
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "product_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Product product;

    public ProductReview(final ProductReview productReview){
        this.setId(productReview.getId());
        this.setContents(productReview.getContents());
        this.setProduct(productReview.getProduct());
        this.setRating(productReview.getRating());
        this.setReviewer(productReview.getReviewer());
    }

    public ProductReview(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReviewer()
    {
        return reviewer;
    }

    public void setReviewer(final String reviewer)
    {
        this.reviewer = reviewer;
    }

    public ProductRating getRating()
    {
        return rating;
    }

    public void setRating(final ProductRating rating)
    {
        this.rating = rating;
    }

    public String getContents()
    {
        return contents;
    }

    public void setContents(final String contents)
    {
        this.contents = contents;
    }

    public Product getProduct()
    {
        return product;
    }

    public void setProduct(final Product product)
    {
        this.product = product;
    }
}
