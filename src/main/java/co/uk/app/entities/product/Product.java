package co.uk.app.entities.product;

import co.uk.app.entities.commerce.Cart;

import javax.persistence.*;
import java.util.List;
import java.util.Set;


/**
 * Created by ${Eclair} on 10/21/2018.
 */
@Entity
@Table(name = "product")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String code;
    private String name;
    private String shortDescription;
    private String htmlDetails;
    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, optional = false)
    private Price price;
//    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, optional = false)
//    private CountDown countDown;
    @ManyToMany(
            fetch= FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            }
    )
    @JoinTable(name="product_vendor",
            joinColumns = {@JoinColumn(name="product_id")}, inverseJoinColumns = {@JoinColumn(name="vendor_id")})
    private Set<Vendor> vendors;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProductReview> productReviews;
    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProductImage> images;
//    @ManyToMany(
//    		fetch= FetchType.LAZY,
//			cascade = {
//    				CascadeType.PERSIST,
//					CascadeType.MERGE
//			}
//	 )
//	 @JoinTable(name="product_location",
//	 	joinColumns = {@JoinColumn(name="product_id")}, inverseJoinColumns = {@JoinColumn(name="location_id")})
//    private Set<ProductLocation> locations;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cart_id")
    private Cart cart;

    public Product(){super();}

    public Product(final Product product) {
        this.setCode(product.getCode());
        this.setName(product.getName());
        this.setPrice(product.getPrice());
        this.setShortDescription(product.getShortDescription());
        this.setHtmlDetails(product.getHtmlDetails());
//        this.setCountDown(product.getCountDown());
        this.setVendors(product.getVendors());
        this.setProductReviews(product.getProductReviews());
        this.setImages(product.getImages());
//        this.setLocations(product.getLocations());
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

//    public CountDown getCountDown() {
//        return countDown;
//    }
//
//    public void setCountDown(CountDown countDown) {
//        this.countDown = countDown;
//    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Vendor> getVendors() {
        return vendors;
    }

    public void setVendors(Set<Vendor> vendors) {
        this.vendors = vendors;
    }

    public List<ProductReview> getProductReviews() {
        return productReviews;
    }

    public void setProductReviews(List<ProductReview> productReviews) {
        this.productReviews = productReviews;
    }

//	public Set<ProductLocation> getLocations()
//	{
//		return locations;
//	}
//
//	public void setLocations(final Set<ProductLocation> locations)
//	{
//		this.locations = locations;
//	}

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getHtmlDetails() {
        return htmlDetails;
    }

    public void setHtmlDetails(String htmlDetails) {
        this.htmlDetails = htmlDetails;
    }

    public List<ProductImage> getImages() {
        return images;
    }

    public void setImages(List<ProductImage> images) {
        this.images = images;
    }

    public Cart getCart() {
        return cart;
    }

    public void setCart(Cart cart) {
        this.cart = cart;
    }
}
