package co.uk.app.entities.product;

import javax.persistence.*;

/**
 * Created by ${Eclair} on 10/30/2018.
 */
@Entity
@Table(name = "image")
public class ProductImage {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String path;
    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    public ProductImage(){super();}

    public ProductImage(final ProductImage image) {
        this.setId(image.getId());
        this.setPath(image.getPath());
        this.setProduct(image.getProduct());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
