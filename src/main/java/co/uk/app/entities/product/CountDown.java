package co.uk.app.entities.product;

import co.uk.app.utilities.CustomerDateAndTimeDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by ${Eclair} on 10/22/2018.
 */
@Entity
@Table(name = "timer")
public class CountDown {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @JsonDeserialize(using=CustomerDateAndTimeDeserialize.class)
    @Temporal(value = TemporalType.TIME)
    private Date startingDate;
    @JsonDeserialize(using=CustomerDateAndTimeDeserialize.class)
    @Temporal(value = TemporalType.TIME)
    private Date endingDate;

    public CountDown() {
    }

    public CountDown(CountDown countDown) {
        this.setStartingDate(countDown.getStartingDate());
        this.setEndingDate(countDown.getEndingDate());
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(Date startingDate) {
        this.startingDate = startingDate;
    }

    public Date getEndingDate() {
        return endingDate;
    }

    public void setEndingDate(Date endingDate) {
        this.endingDate = endingDate;
    }
}
