package co.uk.app.entities.product;

import javax.persistence.*;

/**
 * Created by ${Eclair} on 10/22/2018.
 */
@Entity
@Table(name = "price")
public class Price {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String currency;
    private String rrp;
    private String reducedPrice;

    public Price(){super();}
    public Price(final Price price) {
        this.setId(price.getId());
        this.setCurrency(price.getCurrency());
        this.setRrp(price.getRrp());
        this.setReducedPrice(price.getReducedPrice());
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getRrp() {
        return rrp;
    }

    public void setRrp(String rrp) {
        this.rrp = rrp;
    }

    public String getReducedPrice() {
        return reducedPrice;
    }

    public void setReducedPrice(String reducedPrice) {
        this.reducedPrice = reducedPrice;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
