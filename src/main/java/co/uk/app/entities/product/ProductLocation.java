package co.uk.app.entities.product;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by ${Eclair} on 11/1/2018.
 */
@Entity
@Table(name = "location")
public class ProductLocation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String longitude;
    private String latitude;

//    @ManyToMany(fetch = FetchType.LAZY,
//          cascade = {
//                CascadeType.PERSIST,
//                CascadeType.MERGE
//          }, mappedBy = "locations")
//    private Set<Product> productSet;

    public ProductLocation() {
        super();
    }

    public ProductLocation(final ProductLocation location) {
        this.setId(location.getId());
        this.setLatitude(location.getLatitude());
        this.setLongitude(location.getLongitude());
//        this.setProductSet(location.productSet);
    }

    public ProductLocation(String longitude, String latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public Long getId()
    {
        return id;
    }

    public void setId(final Long id)
    {
        this.id = id;
    }

//    public Set<Product> getProductSet()
//    {
//        return productSet;
//    }
//
//    public void setProductSet(final Set<Product> productSet)
//    {
//        this.productSet = productSet;
//    }
}
