package co.uk.app.utilities;

/**
 * Created by ${Eclair} on 5/26/2019.
 */
public enum OrderStatus {
    NEW_ORDER,
    PAID,
    ARCHIVE_ORDER,
    CANCELED_ORDER
}
