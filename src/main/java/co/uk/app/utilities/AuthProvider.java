package co.uk.app.utilities;

/**
 * Created by ${Eclair} on 6/8/2019.
 */
public enum AuthProvider {
    local,
    facebook,
    google
}
