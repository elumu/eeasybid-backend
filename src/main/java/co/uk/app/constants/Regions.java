package co.uk.app.constants;

/**
 * Created by ${Eclair} on 12/30/2018.
 */
public enum Regions {
    Africa, Americas, Asia, Europe, Oceania
}
