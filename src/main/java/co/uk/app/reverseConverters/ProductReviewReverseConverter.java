package co.uk.app.reverseConverters;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import co.uk.app.entities.product.ProductReview;
import co.uk.app.payloads.ProductReviewDTO;

@Component
public class ProductReviewReverseConverter implements Converter<ProductReview, ProductReviewDTO>
{
	@Override
	public ProductReviewDTO convert(final ProductReview productReview)
	{
		final ProductReviewDTO productReviewDTO = new ProductReviewDTO();
		productReviewDTO.setReviewer(productReview.getReviewer());
//		productReviewDTO.setProductRatingDTO(productReview.getRating());
//		productReviewDTO.setProductDTO(productReview.getProduct());
		productReviewDTO.setContents(productReview.getContents());
		return productReviewDTO;
	}

	public List<ProductReviewDTO> convertAll(final List<ProductReview> productReviewList)
	{
		final List<ProductReviewDTO> productReviewDTOList = new ArrayList<>();
		productReviewList.forEach(productReview ->{
			productReviewDTOList.add(convert(productReview));
		});
		return productReviewDTOList;
	}
}
