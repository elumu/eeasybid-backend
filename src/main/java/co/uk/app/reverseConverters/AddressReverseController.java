package co.uk.app.reverseConverters;

import co.uk.app.entities.common.Address;
import co.uk.app.payloads.AddressDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${Eclair} on 4/25/2019.
 */
@Component
public class AddressReverseController implements Converter<Address, AddressDTO> {
    @Override
    public AddressDTO convert(Address address) {
        final AddressDTO addressDTO = new AddressDTO();
        addressDTO.setAddress1(address.getAddress1());
        addressDTO.setAddress2(address.getAddress2());
        addressDTO.setCity(address.getCity());
        addressDTO.setCountry(address.getCountry());
        addressDTO.setPostcode(address.getPostcode());
        return addressDTO;
    }

    public List<AddressDTO> convertAll(List<Address> addresses){
        final List<AddressDTO> addressDTOList = new ArrayList<>();
        addresses.forEach(address -> {
            addressDTOList.add(convert(address));
        });
        return addressDTOList;
    }
}
