package co.uk.app.reverseConverters;

import co.uk.app.entities.product.Price;
import co.uk.app.payloads.PriceDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by ${Eclair} on 4/25/2019.
 */
@Component
public class PriceReverseConverter implements Converter<Price, PriceDTO> {
    @Override
    public PriceDTO convert(Price price) {
        final PriceDTO priceDTO = new PriceDTO();
        priceDTO.setRrp(price.getRrp());
        priceDTO.setReducedPrice(price.getReducedPrice());
        priceDTO.setCurrency(price.getCurrency());
        return priceDTO;
    }
}
