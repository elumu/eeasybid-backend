package co.uk.app.reverseConverters;

import co.uk.app.entities.common.Contact;
import co.uk.app.payloads.ContactDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${Eclair} on 4/25/2019.
 */
@Component
public class ContactReverseController implements Converter<Contact, ContactDTO> {
    @Override
    public ContactDTO convert(Contact contact) {
        final ContactDTO contactDTO = new ContactDTO();
        contactDTO.setContact(contact.getMobileNumber());
        return contactDTO;
    }

    public List<ContactDTO> convertAll(List<Contact> contacts){
        final List<ContactDTO> contactDTOList = new ArrayList<>();
        contacts.forEach(contact -> contactDTOList.add(convert(contact)));
        return contactDTOList;
    }

}
