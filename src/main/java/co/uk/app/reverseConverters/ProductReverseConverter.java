package co.uk.app.reverseConverters;

import co.uk.app.entities.product.Product;
import co.uk.app.payloads.ProductDTO;
import co.uk.app.payloads.VendorDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

/**
 * Created by ${Eclair} on 4/25/2019.
 */
@Component
public class ProductReverseConverter implements Converter<Product, ProductDTO> {
    final PriceReverseConverter priceReverseConverter;
    final ProductReviewReverseConverter productReviewReverseConverter;
    final VendorReverseConverter vendorReverseConverter;

    @Autowired
    public ProductReverseConverter(PriceReverseConverter priceReverseConverter,
                                   ProductReviewReverseConverter productReviewReverseConverter, VendorReverseConverter vendorReverseConverter) {
        this.priceReverseConverter = priceReverseConverter;
        this.productReviewReverseConverter = productReviewReverseConverter;
        this.vendorReverseConverter = vendorReverseConverter;
    }

    @Override
    public ProductDTO convert(Product product) {
        final ProductDTO productDTO = new ProductDTO();
        productDTO.setCode(product.getCode());
        productDTO.setHtmlDetails(product.getHtmlDetails());
        productDTO.setName(product.getName());
        productDTO.setPrice(priceReverseConverter.convert(product.getPrice()));
        productDTO.setReviews(productReviewReverseConverter.convertAll(product.getProductReviews()));
        productDTO.setShortDescription(product.getShortDescription());
        productDTO.setVendors((Set<VendorDTO>) vendorReverseConverter.convertAll(product.getVendors()));
        return productDTO;
    }

    public Collection<ProductDTO> convertAll(Collection<Product> products){
        final ArrayList<ProductDTO> productDTOCollection = new ArrayList<>();
        products.forEach(product -> productDTOCollection.add(convert(product)));
        return productDTOCollection;
    }
}
