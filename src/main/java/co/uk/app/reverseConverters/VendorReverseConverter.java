package co.uk.app.reverseConverters;

import co.uk.app.entities.product.Vendor;
import co.uk.app.payloads.VendorDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.*;

/**
 * Created by ${Eclair} on 4/25/2019.
 */
@Component
public class VendorReverseConverter implements Converter<Vendor, VendorDTO> {
    private AddressReverseController addressReverseController;
    private ContactReverseController contactReverseController;
    @Autowired
    public VendorReverseConverter(AddressReverseController addressReverseController,
                                  ContactReverseController contactReverseController) {
        this.addressReverseController = addressReverseController;
        this.contactReverseController = contactReverseController;
    }

    @Override
    public VendorDTO convert(Vendor vendor) {
        final VendorDTO vendorDTO = new VendorDTO();
        vendorDTO.setAddress(addressReverseController.convertAll(vendor.getAddress()));
        vendorDTO.setContact(contactReverseController.convertAll(vendor.getContact()));
        vendorDTO.setDescription(vendor.getDescription());
        vendorDTO.setName(vendor.getName());
        return vendorDTO;
    }

    public Set<VendorDTO> convertAll(Collection<Vendor> vendors){
        final Set<VendorDTO> vendorDTOList = new HashSet<>();
        vendors.forEach(vendor -> vendorDTOList.add(convert(vendor)));
        return vendorDTOList;
    }
}
