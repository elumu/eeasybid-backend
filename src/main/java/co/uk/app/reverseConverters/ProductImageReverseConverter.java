package co.uk.app.reverseConverters;

import co.uk.app.entities.product.ProductImage;
import co.uk.app.payloads.ProductImageDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${Eclair} on 4/25/2019.
 */
@Component
public class ProductImageReverseConverter implements Converter<ProductImage, ProductImageDTO> {
    private ProductReverseConverter productReverseConverter;

    @Autowired
    public ProductImageReverseConverter(ProductReverseConverter productReverseConverter) {
        this.productReverseConverter = productReverseConverter;
    }

    @Override
    public ProductImageDTO convert(ProductImage productImage) {
        final ProductImageDTO productImageDTO = new ProductImageDTO();
        productImageDTO.setId(String.valueOf(productImage.getId()));
        productImageDTO.setPath(productImage.getPath());
        productImageDTO.setProduct(productReverseConverter.convert(productImage.getProduct()));
        return productImageDTO;
    }

    public List<ProductImageDTO> convertAll(List<ProductImage> productImageList) {
        final List<ProductImageDTO> productImageDTOList = new ArrayList<>();
        productImageList.forEach(productImage -> {
            productImageDTOList.add(convert(productImage));
        });
        return productImageDTOList;
    }
}
