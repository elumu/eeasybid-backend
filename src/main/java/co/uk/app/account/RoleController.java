package co.uk.app.account;

import co.uk.app.account.entities.Role;
import co.uk.app.payloads.ApiResponseDTO;
import co.uk.app.payloads.RoleRQDTO;
import co.uk.app.repositories.RolesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URI;

/**
 * Created by ${Eclair} on 10/20/2018.
 */
@RestController
@RequestMapping("/api/roles")
public class RoleController {
    @Autowired
    private RolesRepository rolesRepository;
    @PostMapping("/create")
    public ResponseEntity<?> createRole(@Valid @RequestBody RoleRQDTO roleRQDTO)
    {
        Role role = new Role();
        role.setRole(roleRQDTO.getRole());
        Role result = rolesRepository.save(role);
        final URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/api/roles/{role}")
                .buildAndExpand(result.getRole()).toUri();
        return ResponseEntity.created(location)
                .body(new ApiResponseDTO(HttpServletResponse.SC_CREATED, true, "role "+result.getRole()+" created successfully"));
    }
}
