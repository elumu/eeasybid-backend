package co.uk.app.account;

import co.uk.app.exceptions.ResourceNotFoundException;
import co.uk.app.payloads.PhoneNumber;
import co.uk.app.payloads.Verify;
import com.twilio.rest.verify.v2.service.Verification;
import com.twilio.rest.verify.v2.service.VerificationCheck;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * Created by ${Eclair} on 7/20/2019.
 */
@RestController
@RequestMapping("/api/verify")
public class VerifyAccountControler {

    private VerifyPhoneNumbeService verifyPhoneNumbeService;

    public VerifyAccountControler(VerifyPhoneNumbeService verifyPhoneNumbeService) {
        this.verifyPhoneNumbeService = verifyPhoneNumbeService;
    }

    @PostMapping("/send")
    public ResponseEntity<?> sendVerificationCode(@Valid @RequestBody PhoneNumber phoneNumber){
        Verification verification = verifyPhoneNumbeService.sendVerificationToken(phoneNumber);
        if(verification.getStatus().equalsIgnoreCase("pending"))
            return new ResponseEntity<>(verification, HttpStatus.OK);
        throw new ResourceNotFoundException("verification service was not found");
    }

    @PostMapping("/check")
    public ResponseEntity<?> verifyCode(@Valid @RequestBody Verify verify){
        VerificationCheck verificationCheck = verifyPhoneNumbeService.verify(verify);
        if(verificationCheck.getValid().equals(true) &&
                verificationCheck.getStatus().equalsIgnoreCase("approved"))
            return new ResponseEntity<>(verificationCheck, HttpStatus.OK);
        throw new ResourceNotFoundException("verification check service was not found");
    }
}
