package co.uk.app.account;

import co.uk.app.exceptions.ResourceNotFoundException;
import co.uk.app.payloads.PhoneNumber;
import co.uk.app.payloads.Verify;
import com.twilio.rest.verify.v2.service.Verification;
import com.twilio.rest.verify.v2.service.VerificationCheck;

/**
 * Created by ${Eclair} on 7/20/2019.
 */

public interface VerifyPhoneNumbeService{
    Verification sendVerificationToken(PhoneNumber phoneNumber) throws ResourceNotFoundException;
    VerificationCheck verify(Verify token) throws ResourceNotFoundException;
}
