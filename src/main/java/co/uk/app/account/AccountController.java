package co.uk.app.account;

import co.uk.app.account.dtos.CreateAccountDTO;
import co.uk.app.account.services.AccountService;
import co.uk.app.exceptions.AccountCreationException;
import co.uk.app.repositories.UserProfileRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by ${Eclair} on 5/19/2019.
 */
@RestController
@RequestMapping("/api/accounts")
public class AccountController {
    private static final Logger log = LoggerFactory.getLogger(AccountController.class);
    private final AccountService accountService;
    private UserProfileRepository userProfileRepository;

    /**
     * Instantiates a new Account controller.
     *
     * @param accountService        the account service
     * @param userProfileRepository the user profile repository
     */
    @Autowired
    public AccountController(final AccountService accountService,
                             @Qualifier("userProfileRepository") final UserProfileRepository userProfileRepository) {
        this.accountService = accountService;
        this.userProfileRepository = userProfileRepository;
    }

    /**
     * Create account response entity.
     *
     * @param request the request
     * @return the response entity
     */
    @PostMapping("")
    public ResponseEntity<?> createAccount(@RequestBody CreateAccountDTO request) {
        log.info("create account was called");
        return accountService.createAccount(request)
                .map(account -> new ResponseEntity<>("Account created successfully", HttpStatus.CREATED))
                .orElseThrow(() -> new AccountCreationException("Something went wrong unable to create account"));
    }

   /* *//**
     * Gets all accounts.
     *
     * @return a list of user accounts
     *//*
    @GetMapping("/")
    public ResponseEntity<List<UserAccountResource>> getAllUserAccounts() {
        List<Account> allAccounts = accountFacade.findAllAccounts();
        try{
            final UserAccountResource userAccountResource = new UserAccountResource();
            List<UserAccountResource> allUserAccounts = userAccountResource.retrieveAllUserAccounts(allAccounts);
            return new ResponseEntity<>(allUserAccounts, HttpStatus.OK);
        }catch (Exception exception){
            throw new ResourceNotFoundException("No account found", Account.class.getName(), null, null);
        }
    }

    *//**
     * Gets user account by id.
     *
     * @param id the id
     * @return the user account
     *//*
    @GetMapping("/{id}")
    public ResponseEntity<UserAccountResource> getUserAccountById(@PathVariable("id") final Long id) {
        Optional<Account> optionalUserAccount = accountFacade.findAccountById(id);
        if(!optionalUserAccount.isPresent())
            throw new ResourceNotFoundException("No account found", Account.class.getName(), null, null);
        else {
            final Account account = optionalUserAccount.get();
            final UserAccountResource userAccountResource = new UserAccountResource(account);
            return new ResponseEntity<>(userAccountResource, HttpStatus.OK);
        }
    }

    *//**
     * Gets account profile using cookies.
     *
     * @param accessToken the access token
     * @return the account profile
     *//*
    @GetMapping("/profile")
    public ResponseEntity<UserAccountProfileResource> getAccountProfile(@CookieValue("u_aui") final String accessToken) {
        final Optional<UserAccountProfile> optionalUserAccountProfile = accountFacade.getAccountProfile("Bearer "+accessToken);
        if (optionalUserAccountProfile.isPresent()) {
            final UserAccountProfile userAccountProfile = optionalUserAccountProfile.get();

            final UserAccountProfileResource profileResource = new UserAccountProfileResource(userAccountProfile);
            ControllerLinkBuilder linkTo = linkTo(methodOn(this.getClass()).getUserAccountById(userAccountProfile.getId()));
            profileResource.add(linkTo.withRel("user-account"));
            return new ResponseEntity<>(profileResource, HttpStatus.OK);
        }
        throw new ResourceNotFoundException("A profile for this accessToken does not exist",
                UserAccountProfile.class.getName(), accessToken, accessToken);
    }

    *//**
     * Add address response entity.
     *
     * @param accessToken the access token
     * @param addressDTO  the address dto
     * @return the response entity
     *//*
    @PutMapping("/profile/address")
    public ResponseEntity<String> addAddress(@CookieValue("u_aui") final String accessToken,
                                             @Valid @RequestBody final AddressDTO addressDTO) {
        Optional<UserAccountProfile> optionalUserAccountProfile = accountFacade.getAccountProfile("Bearer " + accessToken);
        if(!optionalUserAccountProfile.isPresent()) throw new ResourceNotFoundException("Can't found a profile with that token");
        else{
            UserAccountProfile userAccountProfile = optionalUserAccountProfile.get();
            Set<Address> addresses = userAccountProfile.getAddresses();
            final Address address = accountFacade.createAddress(addressDTO);
            addresses.add(address);
            userProfileRepository.save(userAccountProfile);
            return new ResponseEntity<>("Address created successfully", HttpStatus.OK);
        }
    }

    *//**
     * Details response entity.
     *
     * @param accessToken        the access token
     * @param personalDetailsDTO the personal details dto
     * @return the response entity
     *//*
    @PutMapping("/profile/details")
    public ResponseEntity<String> details(@CookieValue("u_aui") final String accessToken,
                                          @Valid @RequestBody final PersonalDetailsDTO personalDetailsDTO) {
        Optional<UserAccountProfile> optionalUserAccountProfile = accountFacade.getAccountProfile("Bearer " + accessToken);
        if(!optionalUserAccountProfile.isPresent()) throw new ResourceNotFoundException("Can't found a profile with that token");
        else{
            UserAccountProfile userAccountProfile = optionalUserAccountProfile.get();
            userAccountProfile.setDateOfBirth(personalDetailsDTO.getDateOfBirth());
            userAccountProfile.setGender(personalDetailsDTO.getGender());
            userAccountProfile.setPhoneNumber(personalDetailsDTO.getPhoneNumber());
            userProfileRepository.save(userAccountProfile);
            return new ResponseEntity<>("Profile details created successfully", HttpStatus.OK);
        }
    }*/
}
