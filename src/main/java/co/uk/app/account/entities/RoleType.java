package co.uk.app.account.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by ${Eclair} on 11/24/2019.
 */
@AllArgsConstructor
@Getter
public enum RoleType {
    BASIC("basic"),
    ADMIN("admin");

    String roletype;
}
