package co.uk.app.account.entities;

import co.uk.app.entities.common.Address;

import javax.persistence.*;
import java.util.Set;


/**
 * Created by ${Eclair} on 10/21/2018.
 */
@Entity
@Table(name = "profile_account")
public class UserAccountProfile {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @OneToOne()
    @MapsId
    private Account account;
    private String gender;
    private String dateOfBirth;
    private String phoneNumber;
    @ManyToMany(
            fetch = FetchType.LAZY,
            cascade = {
                    CascadeType.PERSIST,
                    CascadeType.MERGE
            }
    )
    @JoinTable(name="profile_account_address",
            joinColumns= {@JoinColumn(name="profile_account_id")},
            inverseJoinColumns = {@JoinColumn(name="address_id")})
    private Set<Address> addresses;

    public UserAccountProfile() {
        super();
    }

    public UserAccountProfile(final UserAccountProfile userAccountProfile){
        this.setAccount(userAccountProfile.getAccount());
        this.setId(userAccountProfile.getId());
        this.setAddresses(userAccountProfile.getAddresses());
        this.setGender(userAccountProfile.getGender());
        this.setDateOfBirth(userAccountProfile.getDateOfBirth());
        this.setPhoneNumber(userAccountProfile.getPhoneNumber());
    }

    public long getId() {
        return id;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }
}
