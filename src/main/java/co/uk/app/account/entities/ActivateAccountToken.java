package co.uk.app.account.entities;

import javax.persistence.*;
import java.util.Date;


/**
 * Created by ${Eclair} on 4/6/2019.
 */
@Entity
@Table(name = "activatetokens")
public class ActivateAccountToken {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String token;
    @OneToOne()
    @MapsId
    private Account account;
    private Date expiryDate;

    public ActivateAccountToken() {
        super();
    }

    public ActivateAccountToken(final String token, final Account account, final Date expiryDate) {
        this.token = token;
        this.account = account;
        this.expiryDate = expiryDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }
}
