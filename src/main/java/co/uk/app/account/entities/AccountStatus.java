package co.uk.app.account.entities;

import co.uk.app.exceptions.ResourceNotFoundException;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

/**
 * Created by ${Eclair} on 11/24/2019.
 */
@Getter
@AllArgsConstructor
public enum AccountStatus {
    ACTIVE("active"),
    NON_ACTIVE("inactive"),
    VERIFIED("verified"),
    NON_VERIFIED("not verified");

    String description;

    public static AccountStatus getStatusByDescription(String description) {
        return Stream
                    .of(AccountStatus.values())
                    .filter(status -> status.description.equalsIgnoreCase(description))
                    .findFirst()
                    .orElseThrow(()->new ResourceNotFoundException("unable to find status"));
    }
}
