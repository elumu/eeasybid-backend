package co.uk.app.account.services;

import co.uk.app.account.dtos.CreateAccountDTO;
import co.uk.app.account.entities.Account;

import java.util.List;
import java.util.Optional;

/**
 * Created by ${Eclair} on 5/19/2019.
 */
public interface AccountService {

    /**
     * Create account.
     *
     * @param createAccountDTO the account dto
     * @return the an optional account if the creation was successful
     */
    Optional<Account> createAccount(CreateAccountDTO createAccountDTO);

    /**
     * Delete account.
     *
     * @param id the id of the account to delete
     */
    void deleteAccount(Long id);

    /**
     * Find all accounts for the system admin.
     *
     * @return the list of all available account
     */
    List<Account> findAllAccounts();

    /**
     * Find account by email optional.
     *
     * @param id the id
     * @return the optional
     */
    Optional<Account> findAccountById(final Long id);

    /**
     * Find account by email optional.
     *
     * @param email the email
     * @return the optional
     */
    Optional<Account> findAccountByEmail(String email);
}
