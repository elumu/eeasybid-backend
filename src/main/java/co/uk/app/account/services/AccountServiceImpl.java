package co.uk.app.account.services;

import co.uk.app.account.converters.AccountConverter;
import co.uk.app.account.dtos.CreateAccountDTO;
import co.uk.app.account.entities.Account;
import co.uk.app.account.entities.AccountStatus;
import co.uk.app.account.entities.Role;
import co.uk.app.account.repositories.AccountRepository;
import co.uk.app.exceptions.ResourceNotFoundException;
import co.uk.app.repositories.RolesRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Created by ${Eclair} on 5/19/2019.
 */
@Service
public class AccountServiceImpl implements AccountService {
    private static final Logger LOGGER = LoggerFactory.getLogger(AccountServiceImpl.class);
    private final AccountRepository accountRepository;
    private final AccountConverter accountConverter;
    private RolesRepository rolesRepository;

    /**
     * Instantiates a new Account service.
     *  @param accountRepository the user account repository
     * @param accountConverter the account dto converter
     * @param rolesRepository
     */
    public AccountServiceImpl(final AccountRepository accountRepository, AccountConverter accountConverter, RolesRepository rolesRepository) {
        this.accountRepository = accountRepository;
        this.accountConverter = accountConverter;
        this.rolesRepository = rolesRepository;
    }

    @Override
    public Optional<Account> createAccount(CreateAccountDTO createAccountDTO) {
        Account account = accountConverter.convert(createAccountDTO);
        if (account != null) {
            account.setRoles(getDefaultRoles());
            account.setStatus(AccountStatus.NON_VERIFIED);
            return Optional.of(accountRepository.save(account));
        }
        return Optional.empty();
    }

    private Set<Role> getDefaultRoles(){
        Optional<Role> optionalRole = rolesRepository.findById(3);
        return optionalRole.map(Collections::singleton)
                .orElseThrow(() -> new ResourceNotFoundException("Unable to find default role"));
    }

    @Override
    public void deleteAccount(Long id) {

    }

    @Override
    public List<Account> findAllAccounts() {
        LOGGER.info("find all accounts");
        try{
            return accountRepository.findAll();
        }catch (final Exception e){
            LOGGER.error("unable to find any accounts", e);
            throw new ResourceNotFoundException("No existing account");
        }
    }

    @Override
    public Optional<Account> findAccountById(Long id) {
        LOGGER.info("find account by id");
        try
        {
            return accountRepository.findById(id);
        }
        catch (final Exception e)
        {
            LOGGER.error("unable to find account by that id", e);
            throw new ResourceNotFoundException("An account with this email address does not exist",
                    Account.class.getName(), "id", id);
        }
    }

    @Override
    public Optional<Account> findAccountByEmail(String email) {
        return accountRepository.findByEmail(email);
    }
}
