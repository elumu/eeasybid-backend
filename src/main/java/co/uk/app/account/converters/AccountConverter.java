package co.uk.app.account.converters;

import co.uk.app.account.dtos.CreateAccountDTO;
import co.uk.app.account.entities.Account;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

/**
 * Created by ${Eclair} on 12/31/2018.
 */
@Component
public class AccountConverter implements Converter<CreateAccountDTO, Account> {

    @Override
    public Account convert(@NonNull final CreateAccountDTO createAccountDTO) {
        final Account account = new Account();
        account.setUsername(createAccountDTO.getUsername());
        account.setBirthDate(convertBirthDateToLocalDate(createAccountDTO.getBirthDate()));
        account.setEmail(createAccountDTO.getEmail());
        account.setGender(createAccountDTO.getGender());
        account.setPassword(createAccountDTO.getPassword());
        account.setPhoneExtension(createAccountDTO.getPhoneExtension());
        account.setPhoneNumber(createAccountDTO.getPhoneNumber());
        return account;
    }

    private LocalDate convertBirthDateToLocalDate(String birthDate) {
        final String[] tokens = birthDate.trim().split("-");
        return LocalDate.of(Integer.parseInt(tokens[0]), Integer.parseInt(tokens[1]), Integer.parseInt(tokens[2]));
    }
}
