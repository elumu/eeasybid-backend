package co.uk.app.account.converters;

import co.uk.app.account.dtos.RoleDto;
import co.uk.app.account.entities.Role;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by ${Eclair} on 11/24/2019.
 */
@Component
public class RoleConverter implements Converter<RoleDto, Role> {
    @Override
    public Role convert(@NonNull RoleDto roleDto) {
        final Role role = new Role();
        role.setRole(roleDto.getRole());
        return role;
    }

    public Set<Role> convertAll(Set<RoleDto> roleDtos) {
        final Set<Role> roles = new HashSet<>();
        roleDtos.forEach(item -> {
            final Role role = new Role();
            role.setRole(item.getRole());
            roles.add(role);
        });
        return roles;
    }
}
