package co.uk.app.account.dtos;

import lombok.Data;

/**
 * Created by ${Eclair} on 12/31/2018.
 */
@Data
public class CreateAccountDTO {
    private String username;
    private String email;
    private String password;
    private String gender;
    private String birthDate;
    private String phoneExtension;
    private String phoneNumber;
}
