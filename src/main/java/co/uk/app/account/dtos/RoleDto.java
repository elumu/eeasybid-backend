package co.uk.app.account.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by ${Eclair} on 11/24/2019.
 */
@Data
@AllArgsConstructor
public class RoleDto {
    String role;
}
