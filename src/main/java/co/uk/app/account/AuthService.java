package co.uk.app.account;

import co.uk.app.account.entities.ActivateAccountToken;
import co.uk.app.account.entities.PasswordResetToken;
import co.uk.app.account.entities.Account;
import co.uk.app.payloads.LoginRequestDTO;
import co.uk.app.payloads.ResetPasswordRequestDTO;
import co.uk.app.payloads.SignUpRequestDTO;
import co.uk.app.payloads.SocialSignUpRequestDTO;
import org.springframework.security.core.Authentication;

import java.util.Optional;


/**
 * Created by ${Eclair} on 4/7/2019.
 */
public interface AuthService {
    /**
     * Create activate token for user account activate account token.
     *
     * @param account the user account
     * @param token       the token
     * @return the activate account token
     */
    ActivateAccountToken createActivateTokenForUserAccount(final Account account, final String token);

    /**
     * Create password reset token for user password reset token.
     *
     * @param account the user account
     * @param token       the token
     * @return the password reset token
     */
    PasswordResetToken createPasswordResetTokenForUser(final Account account, final String token);

    /**
     * Activate account.
     *
     * @param account the user account
     */
    void activateAccount(final Account account);

    /**
     * Delete activate account token.
     *
     * @param activateAccountToken the activate account token
     */
    void deleteActivateAccountToken(final Account activateAccountToken);

    /**
     * Find account by email optional.
     *
     * @param email the email
     * @return the optional
     */
    Optional<Account> findAccountByEmail(final String email);

    /**
     * Find account by email optional.
     *
     * @param id the id
     * @return the optional
     */
    Optional<Account> findAccountById(final Long id);

    /**
     * Account exist boolean.
     *
     * @param email the email
     * @return the boolean
     */
    boolean emailExist(final String email);

    /**
     * Create account user account.
     *
     * @param signUpRequestDTO the sign up request dto
     * @return the user account
     */
    Account createAccount(SignUpRequestDTO signUpRequestDTO);


    /**
     * Create account user account.
     *
     * @param socialSignUpRequestDTO the social sign up request dto
     * @return the user account
     */
    Account createAccount(SocialSignUpRequestDTO socialSignUpRequestDTO);

    /**
     * Authenticate account authentication.
     *
     * @param loginRequestDTO the login request dto
     * @return the authentication
     */
    Authentication authenticate(LoginRequestDTO loginRequestDTO);

    /**
     * Find password reset token optional.
     *
     * @param token the token
     * @return the optional
     */
    Optional<PasswordResetToken> findPasswordResetToken(String token);

    /**
     * Find activate account token optional.
     *
     * @param activateToken the activate token
     * @return the optional
     */
    Optional<ActivateAccountToken>findActivateAccountToken(String activateToken);

    /**
     * Reset account password user account.
     *
     * @param passwordResetToken the password reset token
     * @param resetPassword      the reset password
     * @return the user account
     */
    Account resetAccountPassword(PasswordResetToken passwordResetToken, ResetPasswordRequestDTO resetPassword);

}
