package co.uk.app.account;

import co.uk.app.account.entities.ActivateAccountToken;
import co.uk.app.account.entities.PasswordResetToken;
import co.uk.app.account.entities.Account;
import co.uk.app.exceptions.ResourceNotFoundException;
import co.uk.app.facades.AuthFacade;
import co.uk.app.payloads.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URI;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;


/**
 * Created by ${Eclair} on 8/19/2018.
 */
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    private final AuthFacade authFacade;
    private final JWTTokenProviderService tokenProvider;

    /**
     * Instantiates a new Auth controller.
     *
     * @param authFacade    the auth facade
     * @param tokenProvider the token provider
     */
    @Autowired
    public AuthController(final AuthFacade authFacade,
                          final JWTTokenProviderService tokenProvider) {
        this.authFacade = authFacade;
        this.tokenProvider = tokenProvider;
    }

    /**
     * Register response entity.
     *
     * @param signUpRequestDTO the sign up request dto
     * @return the response entity
     */
    @PostMapping("/signup")
    public ResponseEntity<String> register(@Valid @RequestBody final SignUpRequestDTO signUpRequestDTO) {
        if (authFacade.emailExist(signUpRequestDTO.getEmail()))
            return new ResponseEntity<>("This email already exist!", HttpStatus.UNAUTHORIZED);

        final Account result = authFacade.createAccount(signUpRequestDTO);

        final URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/api/users/{id}")
                .buildAndExpand(result.getId()).toUri();

        //send email
        //final String content = "You are receiving this email because you have created an account on application.com click on" +
              //  "this link to activate your account " + "http://localhost:3000/api/auth/activate/" + result.getActivateAccountToken()
              //  .getToken();
      //  authFacade.sendEmail("Activate Account", content, result);

        return ResponseEntity.created(location).body("account registered");
    }

    /**
     * Activate account response entity.
     *
     * @param token the token
     * @return the response entity
     */
    @PostMapping("/activate/{token}")
    public ResponseEntity<String> activateAccount(@PathVariable("token") final String token) {
        final Optional<ActivateAccountToken> optionalActivateAccountToken = authFacade.findActivateAccountToken(token);
        if (optionalActivateAccountToken.isPresent()) {
            final ActivateAccountToken activateAccountToken = optionalActivateAccountToken.get();
            final long expiryTimeDifference = activateAccountToken.getExpiryDate().getTime() - new Date().getTime();
            if (expiryTimeDifference <= 0)
                return new ResponseEntity<>("Token already expired request a new activation token",HttpStatus.UNAUTHORIZED);
            else {
                final Account account = activateAccountToken.getAccount();
                authFacade.activateAccount(account);
                return new ResponseEntity<>("Account activated successfully", HttpStatus.OK);
            }
        }
        return new ResponseEntity<>("token does not exist", HttpStatus.NOT_FOUND);
    }

    /**
     * Login response entity.
     *
     * @param loginRequestDTO the login request dto
     * @param response        the response
     * @return the response entity
     */
    @PostMapping("/signin")
    public ResponseEntity<?> login(@Valid @RequestBody final LoginRequestDTO loginRequestDTO, HttpServletResponse response) {
        final Optional<Account> optionalUserAccount = authFacade.getUserAccountByEmail(loginRequestDTO.getEmail());
        if (optionalUserAccount.isPresent()) {
            final Account account = optionalUserAccount.get();
           /* if (!account.getIsActive())
                return new ResponseEntity<>(
                        new ApiResponseDTO(HttpServletResponse.SC_UNAUTHORIZED, false,
                                "This account is not activated please activate account before signing in"), HttpStatus.UNAUTHORIZED);*/
            final Authentication authentication = authFacade.authenticate(loginRequestDTO);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            final String jwt = tokenProvider.generateToken(authentication);
            final Date expiryDate = tokenProvider.getExpiryDate();
            createCookie(response, jwt);
            return new ResponseEntity<>(new JWTAuthenticationResponseDTO(jwt, expiryDate), HttpStatus.OK);
        }
        throw new ResourceNotFoundException("An account with this email address does not exist",
                Account.class.getName(), "email", loginRequestDTO.getEmail());
    }

    private void createCookie(HttpServletResponse response, String jwt) {
        final Cookie cookie = new Cookie("ASESSION", jwt);
        cookie.setMaxAge(7 * 24 * 60 * 60); // expires in 7 days
        cookie.setHttpOnly(true);
        cookie.setSecure(true);
        response.addCookie(cookie);
    }

    /**
     * Gets email from user.
     *
     * @param emailRequestDTO the email request dto
     * @return the email from user
     */
    @PostMapping("/getemail")
    public ResponseEntity<?> getEmailFromUser(@Valid @RequestBody final EmailRequestDTO emailRequestDTO) {
        final Optional<Account> optionaluserAccount = authFacade.getUserAccountByEmail(emailRequestDTO.getEmail());
        optionaluserAccount.orElseThrow(() ->
                new ResourceNotFoundException("The account with that email does not exist", Account.class.getName(), "email",
                        emailRequestDTO.getEmail()));
        final Account account = optionaluserAccount.get();
        final String token = UUID.randomUUID().toString();
        authFacade.createPasswordResetTokenForUser(optionaluserAccount.get(), token);
        final String content = "Click on this link to change your password http://localhost:4000/api/auth/resetpassword/" + token;
        //send email using the email service
        authFacade.sendEmail("Reset Your Password", content, account);
        return new ResponseEntity<>(new ApiResponseDTO(HttpServletResponse.SC_OK, true,
                "An email has been sent to you to change your email"), HttpStatus.OK);
    }

    /**
     * Send token response entity.
     *
     * @param token the token
     * @return the response entity
     */
    @PostMapping("/send/{token}")
    public ResponseEntity<PasswordResetTokenResponseDTO> sendToken(@PathVariable("token") final String token) {
        final Optional<PasswordResetToken> passwordResetToken = authFacade.getPasswordResetToken(token);
        final PasswordResetTokenResponseDTO result = new PasswordResetTokenResponseDTO();
        if (passwordResetToken.isPresent()) {
            final long diff = passwordResetToken.get().getExpiryDate().getTime() - new Date().getTime();
            if (diff > 0) {
                result.setTokenIsValid(true);
                result.setTokenIsPresent(true);
                result.setToken(token);
                return ResponseEntity.ok(result);
            }
        } else {
            result.setTokenIsPresent(false);
            result.setTokenIsValid(false);
            result.setToken(null);
        }
        return ResponseEntity.ok(result);
    }

    /**
     * Reset password response entity.
     *
     * @param token         the token
     * @param resetPassword the reset password
     * @return the response entity
     */
    @PutMapping("/resetpassword/{token}")
    public ResponseEntity<String> resetPassword(@PathVariable("token") final String token,
                                           @RequestBody final ResetPasswordRequestDTO resetPassword) {
        final Optional<PasswordResetToken> passwordResetToken = authFacade.getPasswordResetToken(token);
        if (!passwordResetToken.isPresent()) {
            throw new ResourceNotFoundException("Password token not found");
        }
        if (resetPassword.getNewPassword().equals(resetPassword.getConfirmPassword())) {
            final Account result = authFacade.resetAccountPassword(passwordResetToken.get(), resetPassword);
            if (Objects.equals(result.getId(), passwordResetToken.get().getAccount().getId())) {
                return new ResponseEntity<>("password reset successfully", HttpStatus.OK);
            }
        } else {
            return new ResponseEntity<>("password do not much", HttpStatus.BAD_REQUEST);
        }
        return null;
    }

    /**
     * Change password response entity.
     *
     * @param id                 the id
     * @param passwordRequestDTO the password request dto
     * @return the response entity
     */
    @PutMapping("/changepassword/{id}")
    public ResponseEntity<String> changePassword(@PathVariable("id") final Long id,
                                            @RequestBody final ChangePasswordRequestDTO passwordRequestDTO) {
        final Optional<Account> userDocument = authFacade.getUserAccountById(id);
        String response = null;
        userDocument.orElseThrow(() -> new UsernameNotFoundException("user is not found"));
        if (userDocument.get().getPassword().equals(passwordRequestDTO.getOldPassword())) {
            if (passwordRequestDTO.getNewPassword().equals(passwordRequestDTO.getConfirmPassword())) {
                userDocument.get().setPassword(passwordRequestDTO.getNewPassword());
                response = "password changed successfully";
            }
        } else {
            response ="password are not the same";
        }
        assert response != null;
        return ResponseEntity.ok(response);
    }
}
