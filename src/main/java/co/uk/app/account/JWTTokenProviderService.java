package co.uk.app.account;

import org.springframework.security.core.Authentication;

import java.util.Date;


/**
 * The interface Jwt token provider service.
 */
public interface JWTTokenProviderService
{
	/**
	 * Generate token string.
	 *
	 * @param authentication
	 * 		the authentication
	 *
	 * @return the string
	 */
	String generateToken(final Authentication authentication);

	Date getExpiryDate();
	/**
	 * Gets user id from jwt.
	 *
	 * @param token
	 * 		the token
	 *
	 * @return the user id from jwt
	 */
	Long getUserIdFromJWT(final String token);

	/**
	 * Validate token boolean.
	 *
	 * @param authToken
	 * 		the auth token
	 *
	 * @return the boolean
	 */
	boolean validateToken(final String authToken);
}
