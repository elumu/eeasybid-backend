package co.uk.app.account;

import co.uk.app.account.entities.Role;
import co.uk.app.exceptions.AppRoleNotFoundException;

/**
 * Created by ${Eclair} on 8/26/2018.
 */
public interface RoleDetailsService {
    Role loadRoleById(Integer id) throws AppRoleNotFoundException;
}
