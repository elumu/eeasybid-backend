package co.uk.app.account.repositories;

import co.uk.app.account.entities.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


/**
 * Created by ${Eclair} on 8/19/2018.
 */
public interface AccountRepository extends JpaRepository<Account, Long> {
    /**
     * Find by email optional.
     *
     * @param email
     * 		the email
     *
     * @return the optional
     */
    Optional<Account> findByEmail(final String email);

    /**
     * Exists by email boolean.
     *
     * @param email
     * 		the email
     *
     * @return the boolean
     */
    boolean existsByEmail(final String email);
}
