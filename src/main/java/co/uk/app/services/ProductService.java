package co.uk.app.services;

import co.uk.app.entities.product.*;
import co.uk.app.payloads.ProductDTO;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;


/**
 * The interface Product service.
 */
public interface ProductService
{
    /**
     * Upload file.
     *
     * @param file      the file
     * @param productId the product id
     * @return the string
     */
    String uploadFile(final MultipartFile file, final Long productId);

    /**
     * Create product product.
     *
     * @param productDTO the product dto
     * @return the product
     */
    Product createProduct(ProductDTO productDTO);

    /**
     * Create product image product image.
     *
     * @param imagePath the image path
     * @param productId the product id
     * @return the product image
     */
    ProductImage createProductImage(String imagePath, Long productId);

    /**
     * Find price by id.
     *
     * @param priceId the price id
     * @return the optional
     */
    Optional<Price> findPriceById(long priceId);

    /**
     * Find product by id.
     *
     * @param id the id
     * @return the optional
     */
    Optional<Product> findProductById(long id);

    /**
     * Find vendor by id optional.
     *
     * @param id the id
     * @return the optional
     */
    Optional<Vendor> findVendorById(long id);

    /**
     * Find review by id optional.
     *
     * @param id the id
     * @return the optional
     */
    Optional<ProductReview> findReviewById(long id);

    /**
     * Find vendors by product id.
     *
     * @param id the id
     * @return the list of vendors
     */
    List<Vendor> findVendorsByProductId(long id);

    /**
     * Find reviews by product id.
     *
     * @param id the id
     * @return the list of reviews
     */
    List<ProductReview> findReviewsByProductId(long id);

    /**
     * Find product by images id list of images.
     *
     * @param id the id
     * @return the list
     */
    List<ProductImage> findProductByImagesId(long id);

    /**
     * Find all products.
     *
     * @return the list of products
     */
    List<Product> findAllProduct();
}
