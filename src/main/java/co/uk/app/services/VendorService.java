package co.uk.app.services;

import co.uk.app.entities.product.Vendor;
import co.uk.app.payloads.VendorDTO;

import java.util.List;
import java.util.Optional;

/**
 * Created by ${Eclair} on 5/20/2019.
 */
public interface VendorService {
    /**
     * Add vendor.
     *
     * @param vendorDTO the vendor dto
     * @return the vendor
     */
    Vendor addVendor(VendorDTO vendorDTO);

    /**
     * Retrieve vendor by id vendor.
     *
     * @param id the id
     * @return the vendor
     */
    Optional<Vendor> findVendorById(Long id);

    /**
     * Retrieve all vendors list.
     *
     * @return the list
     */
    List<Vendor> findAllVendors();
}
