package co.uk.app.services.impl;

import co.uk.app.entities.commerce.Cart;
import co.uk.app.entities.product.Product;
import co.uk.app.repositories.CartRepository;
import co.uk.app.services.CommerceService;
import co.uk.app.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by ${Eclair} on 5/28/2019.
 */
@Service
public class CommerceServiceImpl implements CommerceService {
    private ProductService productService;
    private CartRepository cartRepository;

    @Autowired
    public CommerceServiceImpl(final ProductService productService,
                               final CartRepository cartRepository) {
        this.productService = productService;
        this.cartRepository = cartRepository;
    }

    @Override
    public Cart createShoppingCart(final Long userId, final List<String> listOfProductId) {
        final Cart shoppingCart = new Cart();
        shoppingCart.setCartOwner(userId);
        shoppingCart.setProducts(populateShoppingCart(listOfProductId));
        return cartRepository.save(shoppingCart);
    }

    @Override
    public Optional<Cart> findCartById(Long cartId) {
        return cartRepository.findById(cartId);
    }

    @Override
    public float computeOrderPrice(Cart shoppingCart) {
        List<Product> products = shoppingCart.getProducts();
        float totalPrice = 0;
        for (Product product : products) {
            totalPrice += Float.valueOf(product.getPrice().getReducedPrice());
        }
        return totalPrice;
    }

    private List<Product> populateShoppingCart(final List<String> listOfProductId){
        final List<Product> productList = new ArrayList<>();
        listOfProductId.forEach(productId ->{
            Optional<Product> optionalProduct = productService.findProductById(Long.valueOf(productId));
            optionalProduct.ifPresent(productList::add);
        });
        return productList;
    }
}
