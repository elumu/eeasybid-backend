package co.uk.app.services.impl;

import co.uk.app.converters.VendorConverter;
import co.uk.app.entities.product.Vendor;
import co.uk.app.payloads.VendorDTO;
import co.uk.app.repositories.VendorRepository;
import co.uk.app.services.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * Created by ${Eclair} on 5/20/2019.
 */
@Service
public class VendorServiceImpl implements VendorService {
    private VendorConverter vendorConverter;
    private VendorRepository vendorRepository;
    @Autowired
    public VendorServiceImpl(final VendorConverter vendorConverter,
                             final VendorRepository vendorRepository) {
        this.vendorConverter = vendorConverter;
        this.vendorRepository = vendorRepository;
    }

    @Override
    public Vendor addVendor(VendorDTO vendorDTO) {
        final Vendor vendor = vendorConverter.convert(vendorDTO);
        return vendorRepository.save(vendor);
    }

    @Override
    public Optional<Vendor> findVendorById(Long id) {
        return vendorRepository.findById(id);
    }

    @Override
    public List<Vendor> findAllVendors() {
        return vendorRepository.findAll();
    }
}
