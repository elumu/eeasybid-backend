package co.uk.app.services.impl;

import co.uk.app.exceptions.ResourceNotFoundException;
import co.uk.app.payloads.PhoneNumber;
import co.uk.app.payloads.Verify;
import co.uk.app.account.VerifyPhoneNumbeService;
import com.twilio.Twilio;
import com.twilio.rest.verify.v2.service.Verification;
import com.twilio.rest.verify.v2.service.VerificationCheck;
import org.springframework.stereotype.Service;

/**
 * Created by ${Eclair} on 7/20/2019.
 */
@Service
public class VerifyPhoneNumberImpl implements VerifyPhoneNumbeService {

    private static final String ACCOUNT_SID = "AC9482f2f2a69a87724d437bea327bc493";
    private static final String AUTH_TOKEN = "71b354ee6126af7de64f693605001ddb";
    private static final String pathServiceSid = "VA278e8784aa00de2a5600bf7b3be85de2";

    @Override
    public Verification sendVerificationToken(PhoneNumber phoneNumber) throws ResourceNotFoundException {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        return Verification.creator(pathServiceSid, phoneNumber.getPhoneNumber(), "sms").create();
    }

    @Override
    public VerificationCheck verify(Verify verification) throws ResourceNotFoundException{
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
       VerificationCheck verificationCheck = VerificationCheck.creator(
                pathServiceSid,
                verification.getToken()
        ).setTo(verification.getPhoneNumber()).create();
       return verificationCheck;
    }
}
