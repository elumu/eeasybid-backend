package co.uk.app.services.impl;

import co.uk.app.configs.CloudinaryConfig;
import co.uk.app.converters.ProductConverter;
import co.uk.app.entities.product.*;
import co.uk.app.exceptions.FileStorageException;
import co.uk.app.payloads.ProductDTO;
import co.uk.app.repositories.*;
import co.uk.app.services.ProductService;
import com.cloudinary.utils.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.social.ResourceNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Optional;

//import com.cloudinary.utils.ObjectUtils;

@Service
public class ProductServiceImpl implements ProductService
{
	private CloudinaryConfig cloudinaryConfig;
	private final ProductRepository productRepository;
	private final ProductImageRepository productImageRepository;
	private final ProductConverter productConverter;
	private final PriceRepository priceRepository;
	private final VendorRepository vendorRepository;
	private final ProductReviewRepository productReviewRepository;

	@Autowired
	public ProductServiceImpl(final CloudinaryConfig cloudinaryConfig,
			@Qualifier("productRepository") final ProductRepository productRepository,
			final ProductImageRepository productImageRepository,
			final ProductConverter productConverter,
			final PriceRepository priceRepository,
			final VendorRepository vendorRepository,
			final ProductReviewRepository productReviewRepository
	){
		this.cloudinaryConfig = cloudinaryConfig;
		this.productRepository = productRepository;
		this.productImageRepository = productImageRepository;
		this.productConverter = productConverter;
        this.priceRepository = priceRepository;
		this.vendorRepository = vendorRepository;
		this.productReviewRepository = productReviewRepository;
	}

	@Override
	public String uploadFile(final MultipartFile file, final Long productId)
	{
		if (file.isEmpty()){
			throw new FileStorageException("This file can not be uploaded");
		}
		try {
			final Map uploadResult =  cloudinaryConfig.upload(file.getBytes(),
					ObjectUtils.asMap("public_id", "product-image/"+productId));
			return uploadResult.get("url").toString();
		} catch (IOException e){
			throw new FileStorageException("This file can not be uploaded", e);
		}
	}

	@Override
	public Product createProduct(final ProductDTO productDTO)
	{
		final Product product = productConverter.convert(productDTO);
		return productRepository.save(product);
	}

	@Override
	public ProductImage createProductImage(String imagePath, Long productId) {
		ProductImage productImage = new ProductImage();
		Optional<Product> product = productRepository.findById(productId);
		if(!product.isPresent()){
			throw new ResourceNotFoundException("Cannot find product with id "+ productId, Product.class.getName());
		}
		productImage.setProduct(product.get());
		productImage.setPath(imagePath);
		return productImageRepository.save(productImage);
	}

    @Override
    public Optional<Price> findPriceById(long priceId) {
        return priceRepository.findById(priceId);
    }

	@Override
	public Optional<Product> findProductById(final long id)
	{
		return productRepository.findById(id);
	}

	@Override
	public Optional<Vendor> findVendorById(final long id)
	{
		return vendorRepository.findById(id);
	}

//	@Override
//	public Optional<ProductLocation> findLocationById(final long id)
//	{
//		return productLocationRepository.findById(id);
//	}

	@Override
	public Optional<ProductReview> findReviewById(final long id)
	{
		return productReviewRepository.findById(id);
	}

	@Override
	public List<Vendor> findVendorsByProductId(final long id)
	{
		return vendorRepository.findByProducts_id(id);
	}

	@Override
	public List<ProductReview> findReviewsByProductId(long id) {
		return null;
	}

	@Override
	public List<ProductImage> findProductByImagesId(long id) {
		return productImageRepository.findByProductId(String.valueOf(id));
	}

	@Override
	public List<Product> findAllProduct() {
		return productRepository.findAll();
	}
}
