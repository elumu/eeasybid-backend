package co.uk.app.services.impl;

import co.uk.app.exceptions.OAuth2AuthenticationProcessingException;
import co.uk.app.security.oauth2.account.FacebookOAuth2AccountInfo;
import co.uk.app.security.oauth2.account.GoogleOAuth2AccountInfo;
import co.uk.app.security.oauth2.account.OAuth2AccountInfo;
import co.uk.app.utilities.AuthProvider;

import java.util.Map;

/**
 * Created by ${Eclair} on 6/10/2019.
 */
public class OAuth2UserInfoFactory {
    public static OAuth2AccountInfo getOAuth2UserInfo(String registrationId, Map<String, Object> attributes) {
        if(registrationId.equalsIgnoreCase(AuthProvider.google.toString())) {
            return new GoogleOAuth2AccountInfo(attributes);
        } else if (registrationId.equalsIgnoreCase(AuthProvider.facebook.toString())) {
            return new FacebookOAuth2AccountInfo(attributes);
        } else {
            throw new OAuth2AuthenticationProcessingException("Sorry! Login with " + registrationId + " is not supported yet.");
        }
    }
}
