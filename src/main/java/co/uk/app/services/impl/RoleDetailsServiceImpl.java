package co.uk.app.services.impl;

import co.uk.app.account.entities.Role;
import co.uk.app.exceptions.AppRoleNotFoundException;
import co.uk.app.repositories.RolesRepository;
import co.uk.app.account.RoleDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by ${Eclair} on 8/26/2018.
 */
@Service("roleDetailsService")
public class RoleDetailsServiceImpl implements RoleDetailsService {
    @Qualifier("roleRepository")
    @Autowired
    private RolesRepository rolesRepository;
    @Override
    public Role loadRoleById(Integer id) throws AppRoleNotFoundException{
       Optional<Role> role = rolesRepository.findById(id);
       role.orElseThrow(() -> new AppRoleNotFoundException("role is not found"));
       return role.get();
    }
}
