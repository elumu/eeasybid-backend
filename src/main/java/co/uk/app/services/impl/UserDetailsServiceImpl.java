package co.uk.app.services.impl;

/**
 * Created by ${Eclair} on 4/7/2019.
 */

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.uk.app.account.entities.Account;
import co.uk.app.account.repositories.AccountRepository;
import co.uk.app.security.Principal;


/**
 * Created by ${Eclair} on 8/21/2018.
 */
@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService
{

	private final AccountRepository accountRepository;

	@Autowired
	public UserDetailsServiceImpl(final AccountRepository accountRepository)
	{
		this.accountRepository = accountRepository;
	}

	@Override
	@Transactional
	public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException
	{
		final Optional<Account> user = accountRepository.findByEmail(email);
		user.orElseThrow(() -> new UsernameNotFoundException("user not found"));

		return Principal.create(user.get());
	}

	/**
	 * Load by user id user details.
	 *
	 * @param id
	 * 		the id
	 *
	 * @return the user details
	 */
	@Transactional
	public UserDetails loadByUserId(final Long id)
	{
		final Account account = accountRepository.findById(id).orElseThrow(
				() -> new UsernameNotFoundException("User not found with id " + id)
		);

		return Principal.create(account);
	}
}
