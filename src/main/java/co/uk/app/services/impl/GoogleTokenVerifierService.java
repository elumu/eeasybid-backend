package co.uk.app.services.impl;

import co.uk.app.configs.AppProperties;
import co.uk.app.exceptions.InvalidTokenException;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

/**
 * Created by ${Eclair} on 6/13/2019.
 */
@Service
public class GoogleTokenVerifierService {

    private AppProperties appProperties;

    @Autowired
    public GoogleTokenVerifierService(AppProperties appProperties) {
        this.appProperties = appProperties;
    }

    public Payload verifyIdToken(String idToken) throws GeneralSecurityException, IOException {
        final String CLIENT_ID = appProperties.getOAuth2().getGoogleAppId();
        HttpTransport transport = new NetHttpTransport();
        JsonFactory jsonFactory = new JacksonFactory();
        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(transport, jsonFactory)
                .setAudience(Collections.singletonList(CLIENT_ID))
                .build();

        GoogleIdToken googleIdToken = verifier.verify(idToken);
        if (googleIdToken != null) return googleIdToken.getPayload();
        throw new InvalidTokenException("Invalid ID token.");
    }

}
