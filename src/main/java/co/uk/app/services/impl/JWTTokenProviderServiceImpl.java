package co.uk.app.services.impl;

import co.uk.app.security.Principal;
import co.uk.app.account.JWTTokenProviderService;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.util.Date;


/**
 * Created by ${Eclair} on 8/27/2018.
 */
@Service
public class JWTTokenProviderServiceImpl implements JWTTokenProviderService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(JWTTokenProviderServiceImpl.class);

	@Value("${app.jwtSecret}")
	private String jwtSecret;
	@Value("${app.jwtExpirationTime}")
	private int jwtExpirationTime;

	private Date expiryDate = null;

	/**
	 * Implementation of generate token
	 */
	public String generateToken(final Authentication authentication)
	{
		final Principal userPrincipal = (Principal) authentication.getPrincipal();

		final Date now = new Date();
		expiryDate = new Date(now.getTime() + jwtExpirationTime);

		return Jwts.builder()
				.setSubject(Long.toString(userPrincipal.getId()))
				.setIssuedAt(new Date())
				.setExpiration(expiryDate)
				.signWith(SignatureAlgorithm.HS512, jwtSecret)
				.compact();
	}

	public Date getExpiryDate(){
		return expiryDate;
	}
	/**
	 * Implementation of geting id from jwt
	 */
	public Long getUserIdFromJWT(final String token)
	{
		final Claims claims = Jwts.parser()
				.setSigningKey(jwtSecret)
				.parseClaimsJws(token)
				.getBody();

		return Long.parseLong(claims.getSubject());
	}

	/**
	 * Implementation of validate jwt
	 */
	public boolean validateToken(final String authToken)
	{
		try
		{
			Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
			return true;
		}
		catch (final SignatureException ex)
		{
			LOGGER.error("Invalid JWT signature", ex);
		}
		catch (final MalformedJwtException ex)
		{
			LOGGER.error("Invalid JWT token", ex);
		}
		catch (final ExpiredJwtException ex)
		{
			LOGGER.error("Expired JWT token", ex);
		}
		catch (final UnsupportedJwtException ex)
		{
			LOGGER.error("Unsupported JWT token", ex);
		}
		catch (final IllegalArgumentException ex)
		{
			LOGGER.error("JWT claims string is empty.", ex);
		}
		return false;
	}
}
