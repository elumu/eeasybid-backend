package co.uk.app.services.impl;

import co.uk.app.account.entities.Account;
import co.uk.app.services.EmailService;
import co.uk.app.utilities.Mail;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.util.Date;

/**
 * Created by ${Eclair} on 9/4/2018.
 */
@Service("emailService")
public class EmailServiceImpl implements EmailService {
    private static final Logger LOGGER = LoggerFactory.getLogger(EmailServiceImpl.class);
    @Value("${spring.mail.username}")
    private String FROM;

    private JavaMailSender javaMailSender;

//    // final MimeMessage mimeMailMessage = javaMailSender.createMimeMessage();
//   // final MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMailMessage, true);
    @Autowired
    public EmailServiceImpl(final JavaMailSender javaMailSender) throws MessagingException {
        this.javaMailSender = javaMailSender;
    }

    public EmailServiceImpl() throws MessagingException {

    }

    @Override
    public void sendMessage(final String subject, final String content, final Account account)
    {
        LOGGER.info("Sending email without attachment");
        final Mail mail = new Mail(FROM, account.getEmail(), subject, content);
        final SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(mail.getTo());
        message.setSubject(mail.getSubject());
        message.setText(mail.getContent());
        message.setSentDate(new Date());
        javaMailSender.send(message);
    }
    @Override
    public void sendMessageWithAttachment(final Mail mail) throws MessagingException {
       /* mimeMessageHelper.setToDate(mail.getToDate());
        mimeMessageHelper.setSubject(mail.getSubject());
        mimeMessageHelper.setText(mail.getContent());
        mimeMessageHelper.setSentDate(new Date());

        FileSystemResource fileSystemResource = new FileSystemResource(new File(mail.getAttachementPath()));
        mimeMessageHelper.addAttachment("Attachment", fileSystemResource);
        javaMailSender.send(mimeMailMessage);*/
    }
}
