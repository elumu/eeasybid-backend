package co.uk.app.services.impl;

import co.uk.app.account.entities.*;
import co.uk.app.exceptions.AppRoleNotFoundException;
import co.uk.app.exceptions.ResourceNotFoundException;
import co.uk.app.payloads.LoginRequestDTO;
import co.uk.app.payloads.ResetPasswordRequestDTO;
import co.uk.app.payloads.SignUpRequestDTO;
import co.uk.app.payloads.SocialSignUpRequestDTO;
import co.uk.app.repositories.ActivateAccountTokenRepository;
import co.uk.app.repositories.PasswordTokenRepository;
import co.uk.app.account.repositories.AccountRepository;
import co.uk.app.account.AuthService;
import co.uk.app.account.RoleDetailsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;


/**
 * Created by ${Eclair} on 4/7/2019.
 */
@Service("authService")
public class AuthServiceImpl implements AuthService
{
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthServiceImpl.class);
	private final AccountRepository accountRepository;
	private final ActivateAccountTokenRepository activateAccountTokenRepository;
	private final PasswordTokenRepository passwordTokenRepository;
	private final RoleDetailsService roleDetailsService;
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	private final AuthenticationManager authenticationManager;

	@Value("${app.resetPasswordTokenInMilliseconds}")
	private Long resetPasswordTokenInMilliseconds;

	public AuthServiceImpl(final AccountRepository accountRepository,
			@Qualifier("activateAccountTokenRepository") final ActivateAccountTokenRepository activateAccountTokenRepository,
			final PasswordTokenRepository passwordTokenRepository,
			final RoleDetailsService roleDetailsService,
			final BCryptPasswordEncoder bCryptPasswordEncoder,
			final AuthenticationManager authenticationManager)
	{
		this.accountRepository = accountRepository;
		this.activateAccountTokenRepository = activateAccountTokenRepository;
		this.passwordTokenRepository = passwordTokenRepository;
		this.roleDetailsService = roleDetailsService;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.authenticationManager = authenticationManager;
	}

	@Override
	public ActivateAccountToken createActivateTokenForUserAccount(final Account account, final String token)
	{
		LOGGER.info("Generating activate token");
		final Date now = new Date();
		final Date expiryDate = new Date(now.getTime() + resetPasswordTokenInMilliseconds);
		final ActivateAccountToken activateAccountToken = new ActivateAccountToken(token, account, expiryDate);
		LOGGER.info("Activate token generated");
		return activateAccountToken;
	}

	public PasswordResetToken createPasswordResetTokenForUser(final Account account, final String token)
	{
		LOGGER.info("Generating reset password token");
		return passwordTokenRepository.save(new PasswordResetToken(token, account,
				new Date(new Date().getTime() + resetPasswordTokenInMilliseconds)));
	}

	@Override
	public void activateAccount(Account account)
	{
		LOGGER.info("activating account");
		try
		{
			accountRepository.save(account);
			LOGGER.info("Account activated");
		}
		catch (Exception e)
		{
			LOGGER.error("There's been a problem activating the account", e);
		}
	}

	@Override
	public void deleteActivateAccountToken(final Account account)
	{
		LOGGER.info("Deleting activate account token");
		/*try
		{
			final ActivateAccountToken activateAccountToken = account.getActivateAccountToken();
			account.setActivateAccountToken(null);
			accountRepository.save(account);
			activateAccountTokenRepository.delete(activateAccountToken);
			LOGGER.info("Activate account token deleted successfully");
		}
		catch (Exception e)
		{
			LOGGER.error("Unable to delete the activate account token", e);
		}*/
	}

	@Override
	public Optional<Account> findAccountByEmail(final String email)
	{
		LOGGER.info("find account by email");
		try
		{
			return accountRepository.findByEmail(email);
		}
		catch (final Exception e)
		{
			LOGGER.error("unable to find account by that email", e);
			throw new ResourceNotFoundException("An account with this email address does not exist",
					Account.class.getName(), "email", email);
		}
	}

	@Override
	public Optional<Account> findAccountById(final Long id)
	{
		LOGGER.info("find account by email");
		try
		{
			return accountRepository.findById(id);
		}
		catch (final Exception e)
		{
			LOGGER.error("unable to find account by that email", e);
			throw new ResourceNotFoundException("An account with this email address does not exist",
					Account.class.getName(), "id", id);
		}
	}

	@Override
	public boolean emailExist(final String email)
	{
		return accountRepository.existsByEmail(email);
	}

	@Override
	public Account createAccount(final SignUpRequestDTO signUpRequestDTO)
	{

		final Account account = new Account();
		account.setEmail(signUpRequestDTO.getEmail());
		//account.setFirstName(signUpRequestDTO.getFirstName());
		//account.setLastName(signUpRequestDTO.getLastName());
		account.setPassword(bCryptPasswordEncoder.encode(signUpRequestDTO.getPassword()));
		saveUserAccount(account);
		return accountRepository.save(account);
	}

	@Override
	public Account createAccount(SocialSignUpRequestDTO socialSignUpRequestDTO) {
		final Account account = new Account();
		account.setEmail(socialSignUpRequestDTO.getEmail());
		//account.setFirstName(socialSignUpRequestDTO.getFirstName());
		//account.setLastName(socialSignUpRequestDTO.getLastName());
		account.setPassword(bCryptPasswordEncoder.encode("hardcodedPassword"));
		saveUserSocialAccount(account);
		return accountRepository.save(account);
	}

	@Override
	public Authentication authenticate(final LoginRequestDTO loginRequestDTO)
	{
		final Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(
						loginRequestDTO.getEmail(),
						loginRequestDTO.getPassword()
				)
		);
		return authentication;
	}

	@Override
	public Optional<PasswordResetToken> findPasswordResetToken(final String token)
	{
		return passwordTokenRepository.findByToken(token);
	}

	@Override
	public Optional<ActivateAccountToken> findActivateAccountToken(final String activateToken)
	{
		if (StringUtils.hasText(activateToken))
		{
			return activateAccountTokenRepository.findByToken(activateToken);
		}
		return Optional.empty();
	}

	@Override
	public Account resetAccountPassword(final PasswordResetToken passwordResetToken,
                                        final ResetPasswordRequestDTO resetPassword)
	{
		final Account account = passwordResetToken.getAccount();
		account.setPassword(resetPassword.getNewPassword());
		passwordTokenRepository.delete(passwordResetToken);
		return accountRepository.save(account);
	}

	private void saveUserAccount(final Account account)
	{
		LOGGER.info("creating user account");
		createAccountRole(account);
		createActivateAccountToken(account);
		createInitialAccountProfile(account);
		//account.setIsActive(false);
		//account.setCreatedDate(new Date());
	}

	private void saveUserSocialAccount(final Account account){
		LOGGER.info("creating user account");
		createAccountRole(account);
		createInitialAccountProfile(account);
		//account.setIsActive(true);
		//account.setCreatedDate(new Date());
	}

	private void createInitialAccountProfile(final Account account)
	{
		final UserAccountProfile userAccountProfile = new UserAccountProfile();
		userAccountProfile.setAccount(account);
		//account.setAccountProfile(userAccountProfile);
	}

	private void createActivateAccountToken(final Account account)
	{
		final String token = UUID.randomUUID().toString();
		final ActivateAccountToken activateAccountToken = createActivateTokenForUserAccount(account, token);
		//account.setActivateAccountToken(activateAccountToken);
	}

	private void createAccountRole(final Account account)
	{
		//set roles
		final Set<Role> roles = new HashSet<>();
		final Role role = getRole(3);
		roles.add(role);
		account.setRoles(roles);
	}

	private Role getRole(final Integer id)
	{
		try
		{
			return roleDetailsService.loadRoleById(id);
		}
		catch (AppRoleNotFoundException e)
		{
			LOGGER.error("There was a problem in retrieving the role", e);
		}
		return null;
	}
}
