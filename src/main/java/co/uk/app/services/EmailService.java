package co.uk.app.services;

import javax.mail.MessagingException;

import co.uk.app.account.entities.Account;
import co.uk.app.utilities.Mail;

/**
 * Created by ${Eclair} on 9/4/2018.
 */
public interface EmailService {
     void sendMessage(final String subject, final String content, final Account account);
     void sendMessageWithAttachment(Mail mesage) throws MessagingException;
}
