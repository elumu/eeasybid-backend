package co.uk.app.exceptions;

/**
 * Created by ${Eclair} on 8/26/2018.
 */
public class AppRoleNotFoundException extends Exception  {
    public AppRoleNotFoundException(String role_is_not_found) {
        super(role_is_not_found);
    }
}
