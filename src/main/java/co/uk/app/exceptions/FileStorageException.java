package co.uk.app.exceptions;

/**
 * Created by ${Eclair} on 10/30/2018.
 */
public class FileStorageException extends RuntimeException {
    public FileStorageException(String message) {
        super(message);
    }

    public FileStorageException(String message, Throwable cause) {
        super(message, cause);
    }
}
