package co.uk.app.exceptions;

import org.springframework.security.core.AuthenticationException;

/**
 * Created by ${Eclair} on 6/10/2019.
 */
public class OAuth2AuthenticationProcessingException extends AuthenticationException {
    public OAuth2AuthenticationProcessingException(String msg, Throwable t) {
        super(msg, t);
    }

    public OAuth2AuthenticationProcessingException(String msg) {
        super(msg);
    }
}
