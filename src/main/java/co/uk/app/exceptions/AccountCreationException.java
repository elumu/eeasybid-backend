package co.uk.app.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by ${Eclair} on 11/24/2019.
 */
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class AccountCreationException extends RuntimeException {
    public AccountCreationException(String message) {
        super(message);
    }
}
