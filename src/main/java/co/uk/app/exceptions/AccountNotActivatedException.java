package co.uk.app.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class AccountNotActivatedException extends RuntimeException
{
	public AccountNotActivatedException()
	{
	}

	public AccountNotActivatedException(final String message)
	{
		super(message);
	}

	public AccountNotActivatedException(final String message, final Throwable cause)
	{
		super(message, cause);
	}
}
