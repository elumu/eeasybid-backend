package co.uk.app.resources;

import co.uk.app.account.entities.Role;
import co.uk.app.account.entities.Account;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by ${Eclair} on 5/17/2019.
 */
public class UserAccountResource extends ResourceSupport {
    @JsonProperty
    private long id;
    @JsonProperty
    private String firstName;
    @JsonProperty
    private String lastName;
    @JsonProperty
    private String email;
    @JsonProperty
    private Boolean isActive;
    @JsonProperty
    private Set<RoleResource> roles;

    public UserAccountResource() {
        super();
    }

    public UserAccountResource(final Account model) {
        this.id = model.getId();
        this.email = model.getEmail();
      /*  this.firstName = model.getFirstName();
        this.lastName = model.getLastName();
        this.isActive = model.getIsActive();*/
        this.roles = retrieveAllRoles(model.getRoles());
    }

    public List<UserAccountResource> retrieveAllUserAccounts(final List<Account> list){
        final List<UserAccountResource> userAccountResourceList = new ArrayList<>();
        list.forEach(userAccount -> {
           userAccountResourceList.add(new UserAccountResource(userAccount));
        });
        return userAccountResourceList;
    }

    public Set<RoleResource> retrieveAllRoles(Set<Role> roles){
        Set<RoleResource> roleResources = new HashSet<>();
        roles.forEach(role -> roleResources.add(new RoleResource(role)));
        return roleResources;
    }
}
