package co.uk.app.resources;

import co.uk.app.entities.product.ProductImage;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${Eclair} on 4/26/2019.
 */
public class ProductImageResource extends ResourceSupport {
    @JsonProperty
    private Long id;
    @JsonProperty
    private String path;

    public ProductImageResource() {
    }

    public ProductImageResource(ProductImage productImage) {
        this.id = productImage.getId();
        this.path = productImage.getPath();
    }

    public List<ProductImageResource> getProductImageResources (List<ProductImage> productImageList) {
        List<ProductImageResource> list = new ArrayList<>();
        productImageList.forEach(productImage -> {
            ProductImageResource resource = new ProductImageResource(productImage);
           list.add(resource);
        });
        return list;
    }
}
