package co.uk.app.resources;

import co.uk.app.entities.product.Price;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by ${Eclair} on 4/26/2019.
 */
public class PriceResource extends ResourceSupport {
    @JsonProperty
    private Long id;
    @JsonProperty
    private String currency;
    @JsonProperty
    private String rrp;
    @JsonProperty
    private String reducedPrice;
    public PriceResource(Price price) {
        this.id = price.getId();
        this.currency = price.getCurrency();
        this.reducedPrice = price.getReducedPrice();
        this.rrp = price.getRrp();
    }
}
