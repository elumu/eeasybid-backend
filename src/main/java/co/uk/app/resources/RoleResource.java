package co.uk.app.resources;

import co.uk.app.account.entities.Role;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by ${Eclair} on 5/18/2019.
 */
public class RoleResource extends ResourceSupport {
    @JsonProperty
    private Integer id;
    @JsonProperty
    private String role;

    public RoleResource(){
        super();
    }

    public RoleResource(final Role role){
        this.id = role.getId();
        this.role = role.getRole();
    }
}
