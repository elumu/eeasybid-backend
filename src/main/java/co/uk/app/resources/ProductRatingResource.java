package co.uk.app.resources;

import co.uk.app.entities.product.ProductRating;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by ${Eclair} on 4/26/2019.
 */
public class ProductRatingResource extends ResourceSupport {
    public long id;
    public Integer rating;

    public ProductRatingResource(ProductRating productRating){
        this.id = productRating.id;
        this.rating = productRating.rating;
    }
}
