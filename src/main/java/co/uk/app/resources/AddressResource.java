package co.uk.app.resources;

import co.uk.app.entities.common.Address;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by ${Eclair} on 5/19/2019.
 */
public class AddressResource extends ResourceSupport {
    @JsonProperty
    private Long id;
    @JsonProperty
    private String address1;
    @JsonProperty
    private String address2;
    @JsonProperty
    private String city;
    @JsonProperty
    private String postcode;
    @JsonProperty
    private String country;

    public AddressResource(){
        super();
    }

    public AddressResource(final Address address){
        this.id = address.getId();
        this.address1 = address.getAddress1();
        this.address2 = address.getAddress2();
        this.city = address.getCity();
        this.postcode = address.getPostcode();
        this.country = address.getCountry();
    }
}
