package co.uk.app.resources;

import co.uk.app.entities.product.ProductReview;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;

/**
 * Created by ${Eclair} on 4/26/2019.
 */
public class ProductReviewResource extends ResourceSupport {
    @JsonProperty
    private Long id;
    @JsonProperty
    private String reviewer;
    @JsonProperty
    private ProductRatingResource rating;
    @JsonProperty
    private String contents;

    public ProductReviewResource(ProductReview productReview) {
        this.id = productReview.getId();
        this.reviewer = productReview.getReviewer();
        this.rating = new ProductRatingResource(productReview.getRating());
        this.contents = productReview.getContents();
    }
}
