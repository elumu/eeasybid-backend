package co.uk.app.resources;

import co.uk.app.entities.product.Product;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;

import java.util.List;


public class ProductResource extends ResourceSupport
{
	@JsonProperty
	private long id;
	@JsonProperty
	private String code;
	@JsonProperty
	private String name;
	@JsonProperty
	private String shortDescription;
	@JsonProperty
	private String htmlDetails;
	@JsonProperty
	private PriceResource price;
//	@JsonProperty
//	private Set<Vendor> vendors;
	@JsonProperty
	private List<ProductReviewResource> productReviews;
	@JsonProperty
	private List<ProductImageResource> images;


	public ProductResource(final Product product)
	{
		this.id = product.getId();
		this.code = product.getCode();
		this.name = product.getName();
		this.shortDescription = product.getShortDescription();
		this.htmlDetails = product.getHtmlDetails();
		this.price = new PriceResource(product.getPrice());
//		this.vendors = product.getVendors();
		this.images = new ProductImageResource().getProductImageResources(product.getImages());
//		this.productReviews = product.getProductReviews();
	}
}
