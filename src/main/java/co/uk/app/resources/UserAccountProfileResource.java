package co.uk.app.resources;

import co.uk.app.account.entities.UserAccountProfile;
import co.uk.app.entities.common.Address;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.hateoas.ResourceSupport;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by ${Eclair} on 4/21/2019.
 */
public class UserAccountProfileResource extends ResourceSupport {
    @JsonProperty
    private long id;
    @JsonProperty
    private UserAccountResource userAccountResource;
    @JsonProperty
    private String gender;
    @JsonProperty
    private String dateOfBirth;
    @JsonProperty
    private String phoneNumber;
    @JsonProperty
    private Set<AddressResource> addresses;

    public UserAccountProfileResource(UserAccountProfile model) {
        this.id = model.getId();
        this.userAccountResource = new UserAccountResource(model.getAccount());
        this.gender = model.getGender();
        this.dateOfBirth = model.getDateOfBirth();
        this.phoneNumber = model.getPhoneNumber();
        this.addresses = retrieveAllAddresses(model.getAddresses());
    }

    private Set<AddressResource> retrieveAllAddresses(final Set<Address> addresses){
        final Set<AddressResource> addressResources = new HashSet<>();
        addresses.forEach(address -> addressResources.add(new AddressResource(address)));
        return addressResources;
    }
}
