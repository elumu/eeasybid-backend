package co.uk.app.configs;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${Eclair} on 6/8/2019.
 */
@Configuration
@ConfigurationProperties(prefix = "app")
public class AppProperties {
    private final Auth auth = new Auth();
    private final OAuth2 oAuth2 = new OAuth2();

    public static class Auth{
        private String jwtSecret;
        private long jwtExpirationTime;

        public String getJwtSecret(){
            return jwtSecret;
        }

        public void setJwtSecret(String jwtSecret){
            this.jwtSecret = jwtSecret;
        }

        public long getJwtExpirationTime(){
            return jwtExpirationTime;
        }

        public void setJwtExpirationTime(long jwtExpirationTime) {
            this.jwtExpirationTime = jwtExpirationTime;
        }
    }

    public static final class OAuth2 {
        private String googleAppId;
        private List<String> authorizedRedirectUris = new ArrayList<>();

        public List<String> getAuthorizedRedirectUris() {
            return authorizedRedirectUris;
        }

        public OAuth2 authorizedRedirectUris(List<String> authorizedRedirectUris){
            this.authorizedRedirectUris = authorizedRedirectUris;
            return this;
        }

        public String getGoogleAppId() {
            return googleAppId;
        }

        public void setGoogleAppId(String googleAppId) {
            this.googleAppId = googleAppId;
        }
    }

    public Auth getAuth() {
        return auth;
    }

    public OAuth2 getOAuth2() {
        return oAuth2;
    }
}
