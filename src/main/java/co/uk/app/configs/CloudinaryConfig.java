package co.uk.app.configs;


import co.uk.app.exceptions.AppException;
import com.cloudinary.Cloudinary;
import com.cloudinary.Transformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;


@Component
public class CloudinaryConfig
{
	private static final Logger LOGGER = LoggerFactory.getLogger(CloudinaryConfig.class);
	private Cloudinary cloudinary;
	@Autowired
	public CloudinaryConfig(@Value("${cloudinary.cloud_name}") final String cloudName,
									@Value("${cloudinary.api_key}") final String apiKey,
			@Value("${cloudinary.api_secret}") final String apiSecrete)
	{
		this.cloudinary = new Cloudinary();
		this.cloudinary.config.cloudName=cloudName;
		this.cloudinary.config.apiKey=apiKey;
		this.cloudinary.config.apiSecret=apiSecrete;
	}

	public Map upload(final Object file, final Map options){
		try{
			return this.cloudinary.uploader().upload(file, options);
		}catch (final IOException e){
			LOGGER.error("Unable to upload file to cloudinary ", e);
			throw new AppException("Problem uploading image file", e);
		}
	}

	public String createUrl(final String name, final int width, final int height, final String action){
		return this.cloudinary.url()
				.transformation(new Transformation().width(width).height(height))
				.imageTag(name);
	}
}
