package co.uk.app.configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Created by ${Eclair} on 8/27/2018.
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Override
    public void addCorsMappings(final CorsRegistry registry){
        final long MAX_AGE_SECS = 3600;
        registry.addMapping("/**")
                .allowedOrigins("http://http://127.0.0.1")
                .allowedMethods("HEAD", "OPTIONS", "GET", "POST",
                        "PUT", "PATCH", "DELETE")
                .maxAge(MAX_AGE_SECS);
    }

}
