package co.uk.app.configs;

import co.uk.app.security.JWTAuthenticationEntryPoint;
import co.uk.app.security.filters.JWTAuthenticationFilter;
import co.uk.app.security.oauth2.HttpCookieOAuth2AuthorizationRequestRepository;
import co.uk.app.services.impl.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Optional;


/**
 * Created by ${Eclair} on 8/21/2018.
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
		securedEnabled = true,
		jsr250Enabled = true,
		prePostEnabled = true
)
@EnableJpaAuditing(auditorAwareRef = "auditorProvider")
public class SecurityConfig extends WebSecurityConfigurerAdapter
{
	private final UserDetailsServiceImpl userDetailsService;
	/*private final AccountOAuth2Service accountOAuth2Service;
	private final OAuth2AuthenticationFailureHandler oAuth2AuthenticationFailureHandler;
	private final OAuth2AuthenticationSuccessHandler auth2AuthenticationSuccessHandler;
	private final HttpCookieOAuth2AuthorizationRequestRepository httpCookieOAuth2AuthorizationRequestRepository;*/
	private final BCryptPasswordEncoder bCryptPasswordEncoder;
	private final JWTAuthenticationEntryPoint authenticationEntryPoint;
	private final JWTAuthenticationFilter jwtAuthenticationFilter;

	/**
	 * Instantiates a new Security config.
	 *
	 * @param userDetailsService       the user details service
	 * @param bCryptPasswordEncoder    the b crypt password encoder
	 * @param authenticationEntryPoint the unauthorized handler
	 * @param jwtAuthenticationFilter  the jwt authentication filter
	 */
	@Autowired
	public SecurityConfig(final UserDetailsServiceImpl userDetailsService,
//                          AccountOAuth2Service accountOAuth2Service,
//                          OAuth2AuthenticationFailureHandler oAuth2AuthenticationFailureHandler,
//						  OAuth2AuthenticationSuccessHandler auth2AuthenticationSuccessHandler,
//						  HttpCookieOAuth2AuthorizationRequestRepository httpCookieOAuth2AuthorizationRequestRepository,
                          final BCryptPasswordEncoder bCryptPasswordEncoder,
                          final JWTAuthenticationEntryPoint authenticationEntryPoint,
                          final JWTAuthenticationFilter jwtAuthenticationFilter)
	{
		this.userDetailsService = userDetailsService;
//		this.accountOAuth2Service = accountOAuth2Service;
//        this.oAuth2AuthenticationFailureHandler = oAuth2AuthenticationFailureHandler;
//        this.auth2AuthenticationSuccessHandler = auth2AuthenticationSuccessHandler;
//		this.httpCookieOAuth2AuthorizationRequestRepository = httpCookieOAuth2AuthorizationRequestRepository;
		this.bCryptPasswordEncoder = bCryptPasswordEncoder;
		this.authenticationEntryPoint = authenticationEntryPoint;
		this.jwtAuthenticationFilter = jwtAuthenticationFilter;
	}

	/**
	 * Authentication builder
	 *
	 * @param auth
	 * 		authentication
	 *
	 * @throws Exception
	 * 		throw exceptions
	 */
	@Override
	protected void configure(final AuthenticationManagerBuilder auth) throws Exception
	{
		auth.userDetailsService(userDetailsService)
				.passwordEncoder(bCryptPasswordEncoder);
	}

	@Bean(BeanIds.AUTHENTICATION_MANAGER)
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception
	{
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(final HttpSecurity http) throws Exception
	{
		http.
				cors()
				.and()
				.csrf()
				.disable()
				.exceptionHandling()
				.authenticationEntryPoint(authenticationEntryPoint)
				.and()
				.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.and()
				.authorizeRequests()
				.antMatchers("/",
						"/favicon.ico",
						"/**/*.png",
						"/**/*.gif",
						"/**/*.svg",
						"/**/*.jpg",
						"/**/*.html",
						"/**/*.css",
						"/**/*.js")
				.permitAll()
				.antMatchers("/swagger-resources/**", "/api/**","/v2/**","/**")
					.permitAll()
				.anyRequest()
					.authenticated();
//				.and()
//				.oauth2Login()
//					.authorizationEndpoint()
//						.baseUri("/oauth2/authorize")
//						.authorizationRequestRepository(cookieAuthorizationRequestRepository())
//						.and()
//					.redirectionEndpoint()
//						.baseUri("/oauth2/callback/*")
//						.and()
//				    .userInfoEndpoint()
//						.userService(accountOAuth2Service)
//						.and()
//					.successHandler(auth2AuthenticationSuccessHandler)
//					.failureHandler(oAuth2AuthenticationFailureHandler);
//				.permitAll()
//				.antMatchers("/**")
//				.permitAll()
//				.antMatchers("/api/auth/**")
//				.permitAll()
//				.antMatchers("/api/file/**")
//				.permitAll()
//				.antMatchers("/api/roles/**")
//				.permitAll()
//				.antMatchers("/api/product/")
//				.permitAll()
//				.antMatchers("/fb/**")
//				.permitAll()
//				.antMatchers("/api/user/checkEmailAvailability")
//				.permitAll()
//				.antMatchers(HttpMethod.GET, "/api/users/**")
//				.permitAll()
//				.anyRequest()
//				.authenticated();

		// Add our custom JWT security filter
		http.addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
		// add this line to use H2 web console
		http.headers().frameOptions().disable();
	}

	/**
	 * Cookie authorization request repository http cookie o auth 2 authorization request repository.
	 *
	 * @return the http cookie o auth 2 authorization request repository
	 */
/*
     By default, Spring OAuth2 uses HttpSessionOAuth2AuthorizationRequestRepository to save
     the authorization request. But, since our service is stateless, we can't save it in
     the session. We'll save the request in a Base64 encoded cookie instead.
   */
	@Bean
	public HttpCookieOAuth2AuthorizationRequestRepository cookieAuthorizationRequestRepository() {
		return new HttpCookieOAuth2AuthorizationRequestRepository();
	}


	/**
	 * Cors configuration source cors configuration source.
	 *
	 * @return the cors configuration source
	 */
	@Bean
	CorsConfigurationSource corsConfigurationSource()
	{
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", new CorsConfiguration().applyPermitDefaultValues());
		return source;
	}

	/**
	 * Auditor provider auditor aware.
	 *
	 * @return the auditor aware
	 */
	@Bean
	public AuditorAware<String> auditorProvider() {
		return () -> Optional.ofNullable(SecurityContextHolder.getContext().getAuthentication().getName());
	}
}
