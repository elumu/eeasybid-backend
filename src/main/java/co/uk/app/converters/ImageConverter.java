package co.uk.app.converters;

import co.uk.app.entities.product.ProductImage;
import co.uk.app.payloads.ProductImageDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${Eclair} on 12/1/2018.
 */
@Component
public class ImageConverter implements Converter<ProductImageDTO, ProductImage> {
    @Override
    public ProductImage convert(ProductImageDTO productImageDTO) {
        ProductImage image = new ProductImage();
        return image;
    }

    public List<ProductImage> convertAll(List<ProductImageDTO> list){
        List<ProductImage> productImages = new ArrayList<>();
        list.forEach(productImageDTO -> {
            ProductImage image = convert(productImageDTO);
            productImages.add(image);
        });
        return productImages;
    }

}
