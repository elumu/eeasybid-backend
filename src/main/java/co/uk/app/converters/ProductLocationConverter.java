package co.uk.app.converters;

import co.uk.app.entities.product.ProductLocation;
import co.uk.app.payloads.ProductLocationDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by ${Eclair} on 12/2/2018.
 */
@Component
public class ProductLocationConverter implements Converter<ProductLocationDTO, ProductLocation> {
    @Override
    public ProductLocation convert(final ProductLocationDTO productLocationDTO) {
        final ProductLocation productLocation = new ProductLocation();
        productLocation.setLatitude(productLocationDTO.getLatitude());
        productLocation.setLongitude(productLocationDTO.getLongitude());
        return productLocation;
    }
}
