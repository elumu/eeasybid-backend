package co.uk.app.converters;

import co.uk.app.entities.product.ProductLocation;
import co.uk.app.payloads.ProductLocationDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by ${Eclair} on 4/20/2019.
 */
@Component
public class LocationConverter implements Converter<ProductLocationDTO, ProductLocation> {
    @Override
    public ProductLocation convert(ProductLocationDTO productLocationDTO) {
        final ProductLocation productLocation = new ProductLocation();
        productLocation.setLatitude(productLocationDTO.getLatitude());
        productLocation.setLongitude(productLocationDTO.getLongitude());
        return productLocation;
    }

    public Set<ProductLocation> convertAll(Set<ProductLocationDTO> productLocations) {
        final Set<ProductLocation> set = new HashSet<>();
        productLocations.forEach(productLocation -> {
            set.add(convert(productLocation));
        });
        return set;
    }
}
