package co.uk.app.converters;

import co.uk.app.entities.product.CountDown;
import co.uk.app.payloads.CountDownDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by ${Eclair} on 12/1/2018.
 */
@Component
public class CountDownConverter implements Converter<CountDownDTO, CountDown> {
    @Override
    public CountDown convert(final CountDownDTO source) {
        final CountDown countDown = new CountDown();
        final SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss");
        try {
            countDown.setStartingDate(dateFormat.parse(source.getStartingDate()));
            countDown.setEndingDate(dateFormat.parse(source.getEndingDate()));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return countDown;
    }
}
