package co.uk.app.converters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import co.uk.app.entities.product.Product;
import co.uk.app.payloads.ProductDTO;

/**
 * Created by ${Eclair} on 12/1/2018.
 */
@Component
public class ProductConverter implements Converter<ProductDTO, Product> {
//    private CountDownConverter countDownConverter;
    private PriceConverter priceConverter;
    private VendorConverter vendorConverter;
//    private LocationConverter locationConverter;

    @Autowired
    public ProductConverter(final CountDownConverter countDownConverter,
                            final PriceConverter priceConverter,
                            final VendorConverter vendorConverter,
                            final LocationConverter locationConverter) {
//        this.countDownConverter = countDownConverter;
        this.priceConverter = priceConverter;
        this.vendorConverter = vendorConverter;
//        this.locationConverter = locationConverter;
    }

    @Override
    public Product convert(final ProductDTO productDTO) {
        Product product = new Product();
        product.setCode(productDTO.getCode());
        product.setName(productDTO.getName());
//        product.setCountDown(countDownConverter.convert(productDTO.getCountDown()));
        product.setPrice(priceConverter.convert(productDTO.getPrice()));
        product.setVendors(vendorConverter.convertAll(productDTO.getVendors()));
        product.setHtmlDetails(productDTO.getHtmlDetails());
        product.setShortDescription(productDTO.getShortDescription());
//        product.setLocations(locationConverter.convertAll(productDTO.getLocations()));
        return product;
    }
}
