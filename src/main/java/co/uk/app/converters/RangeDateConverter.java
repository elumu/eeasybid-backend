package co.uk.app.converters;

import co.uk.app.entities.common.RangeDates;
import co.uk.app.payloads.RangeDateDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by ${Eclair} on 12/31/2018.
 */
@Component
public class RangeDateConverter implements Converter<RangeDateDTO, RangeDates> {
    @Override
    public RangeDates convert(final RangeDateDTO rangeDateDTO) {
        final RangeDates rangeDates = new RangeDates();
        rangeDates.setFromDate(rangeDateDTO.getFromDate());
        rangeDates.setToDate(rangeDateDTO.getToDate());
        return rangeDates;
    }
}
