package co.uk.app.converters;

import co.uk.app.entities.common.Contact;
import co.uk.app.payloads.ContactDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${Eclair} on 12/2/2018.
 */
@Component
public class ContactConverter implements Converter<ContactDTO, Contact> {

    @Override
    public Contact convert(ContactDTO contactDTO) {
        final Contact contact = new Contact();
        contact.setMobileNumber(contactDTO.getContact());
        return contact;
    }

    public List<Contact> convertALL(List<ContactDTO> contactDTOList) {
        List<Contact> contactList = new ArrayList<>();
        contactDTOList.forEach(contactDTO -> {
            final Contact contact = convert(contactDTO);
            contactList.add(contact);
        });

        return contactList;
    }
}
