package co.uk.app.converters;

import co.uk.app.entities.product.Vendor;
import co.uk.app.payloads.VendorDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by ${Eclair} on 12/2/2018.
 */
@Component
public class VendorConverter implements Converter<VendorDTO, Vendor> {

    private ContactConverter contactConverter;
    private AddressConverter addressConverter;

    public VendorConverter(final ContactConverter contactConverter,
                           final AddressConverter addressConverter) {
        this.contactConverter = contactConverter;
        this.addressConverter = addressConverter;
    }

    @Override
    public Vendor convert(VendorDTO vendorDTO) {
        final Vendor vendor = new Vendor();
        vendor.setContact(contactConverter.convertALL(vendorDTO.getContact()));
        vendor.setAddress(addressConverter.convertALL(vendorDTO.getAddress()));
        vendor.setDescription(vendorDTO.getDescription());
        vendor.setName(vendorDTO.getName());
        return vendor;
    }

    public Set<Vendor> convertAll(Set<VendorDTO> VendorDTOS){
       final Set<Vendor> set = new HashSet<>();
       VendorDTOS.forEach(VendorDTO -> {
           set.add(convert(VendorDTO));
       });
       return set;
    }
}
