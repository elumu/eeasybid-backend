package co.uk.app.converters;

import co.uk.app.entities.product.Price;
import co.uk.app.payloads.PriceDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by ${Eclair} on 12/1/2018.
 */
@Component
public class PriceConverter implements Converter<PriceDTO, Price> {
    @Override
    public Price convert(PriceDTO priceDTO) {
        Price price = new Price();
        price.setCurrency(priceDTO.getCurrency());
        price.setReducedPrice(priceDTO.getReducedPrice());
        price.setRrp(priceDTO.getRrp());
        return price;
    }
}
