package co.uk.app.converters;

import co.uk.app.entities.common.Address;
import co.uk.app.payloads.AddressDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${Eclair} on 12/2/2018.
 */
@Component
public class AddressConverter implements Converter<AddressDTO, Address> {

    @Override
    public Address convert(AddressDTO addressDTO) {
        Address address = new Address();
        address.setAddress1(addressDTO.getAddress1());
        address.setAddress2(addressDTO.getAddress2());
        address.setCity(addressDTO.getCity());
        address.setCountry(addressDTO.getCountry());
        address.setPostcode(addressDTO.getPostcode());
        return address;
    }

    public List<Address> convertALL(List<AddressDTO> addressDTOList) {
        List<Address> addressList = new ArrayList<>();
        addressDTOList.forEach(addressDTO -> {
            final Address address = convert(addressDTO);
            addressList.add(address);
        });

        return addressList;
    }
}
