package co.uk.app.converters;

import co.uk.app.entities.common.Destination;
import co.uk.app.payloads.DestinationDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ${Eclair} on 12/31/2018.
 */
@Component
public class DestinationConverter implements Converter<DestinationDTO, Destination> {
    private RangeDateConverter rangeDateConverter;

    public DestinationConverter(RangeDateConverter rangeDateConverter) {
        this.rangeDateConverter = rangeDateConverter;
    }

    @Override
    public Destination convert(final DestinationDTO destinationDTO) {
        final Destination destination = new Destination();
        destination.setRegion(destinationDTO.getRegion());
        destination.setCountry(destinationDTO.getCountry());
        destination.setCity(destinationDTO.getCity());
        destination.setRangeDates(rangeDateConverter.convert(destinationDTO.getRangeDateDTO()));
        return destination;
    }

    public List<Destination> convertAll(final List<DestinationDTO> destinations) {
        final List<Destination> destinationList = new ArrayList<>();
        for (DestinationDTO destinationDTO: destinations){
            final Destination destination = new Destination();
            destination.setRegion(destinationDTO.getRegion());
            destination.setCountry(destinationDTO.getCountry());
            destination.setCity(destinationDTO.getCity());
            destination.setRangeDates(rangeDateConverter.convert(destinationDTO.getRangeDateDTO()));
            destinationList.add(destination);
        }
        return destinationList;
    }
}
