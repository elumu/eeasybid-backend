package co.uk.app.converters;

import co.uk.app.account.converters.AccountConverter;
import co.uk.app.entities.travel.TravelPlan;
import co.uk.app.payloads.TravelPlanDTO;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

/**
 * Created by ${Eclair} on 12/31/2018.
 */
@Component
public class TravelPlanConverter implements Converter<TravelPlanDTO, TravelPlan> {
    private AccountConverter accountConverter;
    private DestinationConverter destinationConverter;

    public TravelPlanConverter(AccountConverter accountConverter, DestinationConverter destinationConverter) {
        this.accountConverter = accountConverter;
        this.destinationConverter = destinationConverter;
    }

    @Override
    public TravelPlan convert(TravelPlanDTO travelPlanDTO) {
        final TravelPlan travelPlan = new TravelPlan();
        travelPlan.setAccount(accountConverter.convert(travelPlanDTO.getCreateAccountDTO()));
        travelPlan.setDestinations(destinationConverter.convertAll(travelPlanDTO.getDestinations()));
        return travelPlan;
    }
}
