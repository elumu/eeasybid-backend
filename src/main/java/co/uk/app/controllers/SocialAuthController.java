package co.uk.app.controllers;

import co.uk.app.account.entities.Account;
import co.uk.app.exceptions.InvalidTokenException;
import co.uk.app.facades.AccountFacade;
import co.uk.app.facades.Auth2Facade;
import co.uk.app.facades.AuthFacade;
import co.uk.app.payloads.JWTAuthenticationResponseDTO;
import co.uk.app.payloads.LoginRequestDTO;
import co.uk.app.payloads.SocialRequestDTO;
import co.uk.app.payloads.SocialSignUpRequestDTO;
import co.uk.app.account.JWTTokenProviderService;
import co.uk.app.services.impl.GoogleTokenVerifierService;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Date;
import java.util.Optional;

/**
 * Created by ${Eclair} on 6/13/2019.
 */
@RestController
@RequestMapping("/api/oauth2")
public class SocialAuthController {

    private GoogleTokenVerifierService googleTokenVerifierService;
    private AccountFacade accountFacade;
    private Auth2Facade auth2Facade;
    private AuthFacade authFacade;
    private final JWTTokenProviderService tokenProvider;

    @Autowired
    public SocialAuthController(GoogleTokenVerifierService googleTokenVerifierService, AccountFacade accountFacade,
                                Auth2Facade auth2Facade, AuthFacade authFacade, JWTTokenProviderService tokenProvider) {
        this.googleTokenVerifierService = googleTokenVerifierService;
        this.accountFacade = accountFacade;
        this.auth2Facade = auth2Facade;
        this.authFacade = authFacade;
        this.tokenProvider = tokenProvider;
    }

    @PostMapping("/google-verify")
    public ResponseEntity<?> verifyToken(@RequestBody final SocialRequestDTO socialRequestDTO){
        try {
            Payload payload = googleTokenVerifierService.verifyIdToken(socialRequestDTO.getIdToken());
            Optional<Account> optionalUserAccount = accountFacade.findAccountByEmail(payload.getEmail());
            if(!optionalUserAccount.isPresent()){
                //create account
                final SocialSignUpRequestDTO signUpRequestDTO = new SocialSignUpRequestDTO();
                signUpRequestDTO.setEmail(payload.getEmail());
                signUpRequestDTO.setFirstName((String) payload.get("given_name"));
                signUpRequestDTO.setLastName((String) payload.get("family_name"));
                auth2Facade.createAccount(signUpRequestDTO);
                //perform login
                return new ResponseEntity<>(performAuthentication(payload), HttpStatus.OK);
            }else{
                //perform login
                return new ResponseEntity<>(performAuthentication(payload), HttpStatus.OK);
            }
        } catch (GeneralSecurityException | IOException e) {
            throw new InvalidTokenException("Invalid id token" , e);
        }
    }

    private JWTAuthenticationResponseDTO performAuthentication(Payload payload){
        final LoginRequestDTO loginRequestDTO = new LoginRequestDTO();
        loginRequestDTO.setEmail(payload.getEmail());
        loginRequestDTO.setPassword("hardcodedPassword");
        Authentication authenticate = authFacade.authenticate(loginRequestDTO);
        SecurityContextHolder.getContext().setAuthentication(authenticate);
        final String jwt = tokenProvider.generateToken(authenticate);
        final Date expiryDate = tokenProvider.getExpiryDate();
        return new JWTAuthenticationResponseDTO(jwt, expiryDate);
    }
}
