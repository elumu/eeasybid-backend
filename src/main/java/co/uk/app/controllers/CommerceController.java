package co.uk.app.controllers;

import co.uk.app.account.entities.UserAccountProfile;
import co.uk.app.entities.commerce.Cart;
import co.uk.app.exceptions.ResourceNotFoundException;
import co.uk.app.facades.AccountFacade;
import co.uk.app.facades.CommerceFacade;
import co.uk.app.payloads.ApiResponseDTO;
import co.uk.app.payloads.CartDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import java.net.URI;
import java.util.Optional;

/**
 * Created by ${Eclair} on 5/26/2019.
 */
@RestController
@RequestMapping("/api/commerce")
public class CommerceController {
    private final AccountFacade accountFacade;
    private final CommerceFacade commerceFacade;

    @Autowired
    public CommerceController(final AccountFacade accountFacade,
                              final CommerceFacade commerceFacade) {
        this.accountFacade = accountFacade;
        this.commerceFacade = commerceFacade;
    }

    @PostMapping("/cart")
    public ResponseEntity<?> createCart(@CookieValue("u_aui") final String accessToken, @RequestBody CartDto cartDto)
    {
        final Optional<UserAccountProfile> optionalUserAccountProfile =
                accountFacade.getAccountProfile("Bearer "+accessToken);
        if (optionalUserAccountProfile.isPresent()) {
            final UserAccountProfile userAccountProfile = optionalUserAccountProfile.get();
            Long userId = userAccountProfile.getAccount().getId();

            Cart shoppingCart = commerceFacade.createShoppingCart(userId, cartDto.getListOfProductId());
            final URI location = ServletUriComponentsBuilder
                    .fromCurrentContextPath().path("/api/commerces/{id}")
                    .buildAndExpand(shoppingCart.getId()).toUri();
           return ResponseEntity.created(location).body(
                   new ApiResponseDTO(HttpServletResponse.SC_CREATED, true, "Shopping cart Created Successfully"));
        }
        throw new ResourceNotFoundException("Unable to find account with that token");
    }

    @PostMapping("/order")
    public ResponseEntity<?> createOrder(final Long cartId)
    {
        Optional<Cart> optionalCart = commerceFacade.findCartById(cartId);
        if(optionalCart.isPresent()){
            final Cart shoppingCart = optionalCart.get();
            final float totalPrice = commerceFacade.computerOrderPrice(shoppingCart);
        }
        return null;
    }
}
