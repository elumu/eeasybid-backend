package co.uk.app.controllers;

import co.uk.app.converters.TravelPlanConverter;
import co.uk.app.entities.travel.TravelPlan;
import co.uk.app.payloads.ApiResponseDTO;
import co.uk.app.payloads.TravelPlanDTO;
import co.uk.app.repositories.TravelPlanRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URI;

/**
 * Created by ${Eclair} on 12/31/2018.
 */
@RestController
@RequestMapping("/api/travelplan")
public class TravelPlanController {

    private final TravelPlanRepository travelPlanRepository;
    private final TravelPlanConverter travelPlanConverter;

    @Autowired
    public TravelPlanController(TravelPlanRepository travelPlanRepository, TravelPlanConverter travelPlanConverter) {
        this.travelPlanRepository = travelPlanRepository;
        this.travelPlanConverter = travelPlanConverter;
    }

    @PostMapping("/create")
    public ResponseEntity<?> createTravelPlan(@Valid @RequestBody TravelPlanDTO travelPlanDTO){
       final TravelPlan travelPlan = travelPlanConverter.convert(travelPlanDTO);
       final TravelPlan result = travelPlanRepository.save(travelPlan);
        URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/api/travelplan/{id}")
                .buildAndExpand(result.getId()).toUri();
       return ResponseEntity.created(location).body(new ApiResponseDTO(HttpServletResponse.SC_CREATED, true, "Travel Plan Created Successfully"));
    }
}
