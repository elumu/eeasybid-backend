package co.uk.app.controllers;

import co.uk.app.entities.product.Vendor;
import co.uk.app.exceptions.AppException;
import co.uk.app.exceptions.ResourceNotFoundException;
import co.uk.app.payloads.ApiResponseDTO;
import co.uk.app.payloads.VendorDTO;
import co.uk.app.reverseConverters.VendorReverseConverter;
import co.uk.app.services.VendorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Created by ${Eclair} on 5/20/2019.
 */
@RestController
@RequestMapping("/api/vendors")
public class VendorController {
    private final VendorService vendorService;
    private final VendorReverseConverter vendorReverseConverter;

    @Autowired
    public VendorController(final VendorService vendorService,
                            final VendorReverseConverter vendorReverseConverter) {
        this.vendorService = vendorService;
        this.vendorReverseConverter = vendorReverseConverter;
    }

    @PostMapping("/add")
    public ResponseEntity<?> addVendor(@Valid @RequestBody final VendorDTO vendorDTO){
       try {
           final Vendor vendor = vendorService.addVendor(vendorDTO);
           final URI location = ServletUriComponentsBuilder.fromCurrentContextPath()
                   .path("/api/vendors/{id}")
                   .buildAndExpand(vendor.getId()).toUri();
           return ResponseEntity.created(location)
                   .body(new ApiResponseDTO(HttpServletResponse.SC_CREATED, true, "Vendor added successfully"));
       }catch (Exception e){
           throw new AppException("Something went wrong unnable to create the resource", e);
       }
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getVendorById(@PathVariable("id") final long id){
        final Optional<Vendor> optionalVendor = vendorService.findVendorById(id);
        if(!optionalVendor.isPresent())  throw new ResourceNotFoundException("unable to find the vendor of the given product",
                Vendor.class.getName(), null, null);
        else{
            final Vendor vendor = optionalVendor.get();
            final VendorDTO vendorDTO = vendorReverseConverter.convert(vendor);
            return new ResponseEntity<>(vendorDTO, HttpStatus.OK);
        }

    }
    @GetMapping("/")
    public ResponseEntity<?> getAllVendors(){
        List<Vendor> vendors = vendorService.findAllVendors();
        Set<VendorDTO> vendorDTOS = vendorReverseConverter.convertAll(vendors);
        return new ResponseEntity<>(vendorDTOS, HttpStatus.OK);
    }
}
