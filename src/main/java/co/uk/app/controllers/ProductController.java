package co.uk.app.controllers;

import co.uk.app.entities.product.*;
import co.uk.app.exceptions.ResourceNotFoundException;
import co.uk.app.payloads.*;
import co.uk.app.resources.ProductResource;
import co.uk.app.reverseConverters.ProductImageReverseConverter;
import co.uk.app.reverseConverters.ProductReverseConverter;
import co.uk.app.reverseConverters.ProductReviewReverseConverter;
import co.uk.app.reverseConverters.VendorReverseConverter;
import co.uk.app.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;


/**
 * Created by ${Eclair} on 11/6/2018.
 */
@RestController
@RequestMapping("/api/products")
public class ProductController {

    private final ProductService productService;
    private final ProductReviewReverseConverter productReviewReverseConverter;
    private final ProductImageReverseConverter productImageReverseConverter;
    private final VendorReverseConverter vendorReverseConverter;
    private final ProductReverseConverter productReverseConverter;

    @Autowired
    public ProductController(final ProductService productService,
                             final ProductReviewReverseConverter productReviewReverseConverter,
                             final ProductImageReverseConverter productImageReverseConverter,
                             final VendorReverseConverter vendorReverseConverter,
                             final ProductReverseConverter productReverseConverter) {
        this.productService = productService;
        this.productReviewReverseConverter = productReviewReverseConverter;
        this.productImageReverseConverter = productImageReverseConverter;
        this.vendorReverseConverter = vendorReverseConverter;
        this.productReverseConverter = productReverseConverter;
    }

    @PostMapping("/add")
    public ResponseEntity<ApiResponseDTO> addProduct(@Valid @RequestBody final ProductDTO productDTO){

        final Product result = productService.createProduct(productDTO);
        final URI location = ServletUriComponentsBuilder
                .fromCurrentContextPath().path("/api/products/{code}")
                .buildAndExpand(result.getCode()).toUri();
         return ResponseEntity.created(location).body(new ApiResponseDTO(HttpServletResponse.SC_CREATED, true, "Product Created Successfully"));
    }

    @PostMapping("/addimage")
    public ResponseEntity<?> addProductImage(@RequestParam("files") MultipartFile file, @RequestParam("productId") final
                                             Long productId){
        final String imagePath = productService.uploadFile(file, productId);
        final ProductImage productImage = productService.createProductImage(imagePath, productId);
        final ProductImageDTO productImageDTO = new ProductImageDTO();
        productImageDTO.setId(String.valueOf(productImage.getId()));
        productImageDTO.setPath(productImage.getPath());
        productImageDTO.setProduct(productReverseConverter.convert(productImage.getProduct()));
        return ResponseEntity.ok()
                .body(new ApiResponseDTO(HttpServletResponse.SC_OK, true,
                        "image added to image with id "+productId, productImageDTO));
    }

    @GetMapping("/images/{id}")
    public ResponseEntity<List<ProductImageDTO>> getImagesProductByProductId(@PathVariable("id") final long id)
    {
        List<ProductImage> productImages = productService.findProductByImagesId(id);
        if(CollectionUtils.isEmpty(productImages)) throw new ResourceNotFoundException("Unable to find image for the give image id "+ id,
        ProductImage.class.getName(), null, null);
        else{
            List<ProductImageDTO> productImageDTOList = productImageReverseConverter.convertAll(productImages);
            return new ResponseEntity<>(productImageDTOList, HttpStatus.OK);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductResource> getProductById(@PathVariable("id") final long id)
    {
        final Optional<Product> optionalProduct = productService.findProductById(id);
        if(!optionalProduct.isPresent())
        {
            throw new ResourceNotFoundException("we can not found a product with this id " + id, Product.class.getName(),
                  "product id", Product.class);
        }
        final Product product = optionalProduct.get();
        final ProductResource productResource = new ProductResource(optionalProduct.get());
        addResourcesToProduct(productResource, product);
        return new ResponseEntity<>(productResource, HttpStatus.OK);
    }

    @GetMapping("/")
    public ResponseEntity<List<ProductResource>> getAllProduct()
    {
        final List<Product> products = productService.findAllProduct();
        if(CollectionUtils.isEmpty(products)) throw new ResourceNotFoundException("No product available", Product.class.getName(),
                null, null);
        else{
            final List<ProductResource> productResourceList = new ArrayList<>();
            products.forEach(product -> {
                final ProductResource productResource = new ProductResource(product);
                addResourcesToProduct(productResource, product);
                productResourceList.add(productResource);
            });
            return new ResponseEntity<>(productResourceList, HttpStatus.OK);
        }
    }

    @GetMapping("/price/{id}")
    public ResponseEntity<?> getPriceById(@PathVariable("id") final long id)
    {
        final Optional<Price> optionalPrice = productService.findPriceById(id);
        if (!optionalPrice.isPresent())
        {
            throw new ResourceNotFoundException("unable to find the price of the given product", Price.class.getName(), null, null);
        }
        else
        {
            final Price price = optionalPrice.get();
            final PriceDTO priceDTO = new PriceDTO();
            priceDTO.setCurrency(price.getCurrency());
            priceDTO.setReducedPrice(price.getReducedPrice());
            priceDTO.setRrp(price.getRrp());
            return new ResponseEntity<>(new ApiResponseDTO(HttpServletResponse.SC_OK, true, null,
                  priceDTO), HttpStatus.OK);
        }
    }

    @GetMapping("/vendors/{id}")
    public ResponseEntity<?> getVendorsByProductId(@PathVariable("id") final long id){
        final List<Vendor> vendors = productService.findVendorsByProductId(id);
        if(CollectionUtils.isEmpty(vendors)) throw new ResourceNotFoundException("unable to find the vendor of the given product",
              Vendor.class.getName(), null, null);
        else{
            final Set<VendorDTO> vendorDTOList = vendorReverseConverter.convertAll(vendors);
            return new ResponseEntity<>(new ApiResponseDTO(HttpServletResponse.SC_OK, true, null,
                  vendorDTOList), HttpStatus.OK);
        }
    }

    @GetMapping("/reviews/{id}")
    public ResponseEntity<?> getReviewsByProductId(@PathVariable("id") final long id){
        final List<ProductReview> productReviewList = productService.findReviewsByProductId(id);
        if(CollectionUtils.isEmpty(productReviewList)) throw new ResourceNotFoundException("unable to find the reviews of the given product",
              ProductReview.class.getName(), null, null);
        else{
            final List<ProductReviewDTO> productReviewDTOList = productReviewReverseConverter.convertAll(productReviewList);
            return new ResponseEntity<>(new ApiResponseDTO(HttpServletResponse.SC_OK, true, null,
                  productReviewDTOList), HttpStatus.OK);
        }
    }
    @GetMapping("/review/{id}")
    public ResponseEntity<?> getReviewById(@PathVariable("id") final long id){
        final Optional<ProductReview> optionalProductReview = productService.findReviewById(id);
        if(!optionalProductReview.isPresent())  throw new ResourceNotFoundException("unable to find the review of the given product",
              ProductReview.class.getName(), null, null);
        else{
            final ProductReview productReview = optionalProductReview.get();
            final ProductReviewDTO productReviewDTO = new ProductReviewDTO();
            productReviewDTO.setContents(productReview.getContents());
            productReviewDTO.setReviewer(productReview.getReviewer());
            return new ResponseEntity<>(new ApiResponseDTO(HttpServletResponse.SC_OK, true, null,
                  productReviewDTO), HttpStatus.OK);
        }

    }

    private void addResourcesToProduct(final ProductResource productResource, final Product product){
        final ControllerLinkBuilder linkToPrice = linkTo(methodOn(this.getClass()).getPriceById(product.getPrice().getId()));
        productResource.add(linkToPrice.withRel("product-price"));
        final ControllerLinkBuilder linkToVendors = linkTo(methodOn(this.getClass()).getVendorsByProductId(product.getId()));
        productResource.add(linkToVendors.withRel("product-vendors"));
        final ControllerLinkBuilder linkToReviews = linkTo(methodOn(this.getClass()).getReviewsByProductId(product.getId()));
        productResource.add(linkToReviews.withRel("product-reviews"));
        final ControllerLinkBuilder linkToImages = linkTo(methodOn(this.getClass()).getImagesProductByProductId(product.getId()));
        productResource.add(linkToImages.withRel("product-images"));
    }
}
