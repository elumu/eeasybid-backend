package co.uk.app.payloads;

/**
 * Created by ${Eclair} on 12/1/2018.
 */
public class ProductInfoDTO {
    private String info;

    public void setInfo(String info) {
        this.info = info;
    }

    public String getInfo() {
        return info;
    }
}
