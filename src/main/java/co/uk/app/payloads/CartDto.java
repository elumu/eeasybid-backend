package co.uk.app.payloads;

import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * Created by ${Eclair} on 5/26/2019.
 */
public class CartDto {
    @NotEmpty.List(@NotEmpty)
    List<String> listOfProductId;

    public List<String> getListOfProductId() {
        return listOfProductId;
    }

    public void setListOfProductId(List<String> listOfProductId) {
        this.listOfProductId = listOfProductId;
    }
}
