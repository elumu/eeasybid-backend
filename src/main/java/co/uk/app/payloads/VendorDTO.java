package co.uk.app.payloads;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by ${Eclair} on 12/2/2018.
 */
public class VendorDTO
{
    @NotBlank
    private String name;
    @NotBlank
    private String description;
    @NotNull
    private List<AddressDTO> address;
    private List<ContactDTO> contact;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<AddressDTO> getAddress() {
        return address;
    }

    public void setAddress(List<AddressDTO> address) {
        this.address = address;
    }

    public List<ContactDTO> getContact() {
        return contact;
    }

    public void setContact(List<ContactDTO> contact) {
        this.contact = contact;
    }
}
