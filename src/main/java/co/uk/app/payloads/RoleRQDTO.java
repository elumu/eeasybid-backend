package co.uk.app.payloads;

import javax.validation.constraints.NotBlank;

/**
 * Created by ${Eclair} on 10/20/2018.
 */
public class RoleRQDTO {
    @NotBlank
    private String role;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
