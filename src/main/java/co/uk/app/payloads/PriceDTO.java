package co.uk.app.payloads;

import javax.validation.constraints.NotBlank;

/**
 * Created by ${Eclair} on 11/30/2018.
 */
public class PriceDTO {
    @NotBlank
    private String currency;
    @NotBlank
    private String rrp;
    @NotBlank
    private String reducedPrice;

    public void setRrp(String rrp) {
        this.rrp = rrp;
    }

    public void setReducedPrice(String reducedPrice) {
        this.reducedPrice = reducedPrice;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

    public String getRrp() {
        return rrp;
    }

    public String getReducedPrice() {
        return reducedPrice;
    }
}
