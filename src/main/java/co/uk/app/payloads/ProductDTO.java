package co.uk.app.payloads;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;

/**
 * Created by ${Eclair} on 11/7/2018.
 */
public class ProductDTO {
    @NotBlank
    private String code;
    @NotBlank
    private String name;
    @NotNull
    private PriceDTO price;
    @NotNull
    private String shortDescription;
    @NotNull
    private String htmlDetails;
//    @NotNull
//    private CountDownDTO countDown;
    @NotNull
    private Set<VendorDTO> vendors;
    private List<ProductReviewDTO> reviews;
//    @NotEmpty.List(@NotEmpty)
//    private Set<ProductLocationDTO> locations;

    public void setCode(String code) {
        this.code = code;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(PriceDTO price) {
        this.price = price;
    }

//    public void setCountDown(CountDownDTO countDown) {
//        this.countDown = countDown;
//    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public PriceDTO getPrice() {
        return price;
    }

//    public CountDownDTO getCountDown() {
//        return countDown;
//    }

    public Set<VendorDTO> getVendors() {
        return vendors;
    }

    public void setVendors(Set<VendorDTO> vendors) {
        this.vendors = vendors;
    }

    public List<ProductReviewDTO> getReviews() {
        return reviews;
    }

    public void setReviews(List<ProductReviewDTO> reviews) {
        this.reviews = reviews;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getHtmlDetails() {
        return htmlDetails;
    }

    public void setHtmlDetails(String htmlDetails) {
        this.htmlDetails = htmlDetails;
    }
    //    public Set<ProductLocationDTO> getLocations() {
//        return locations;
//    }
//
//    public void setLocations(Set<ProductLocationDTO> locations) {
//        this.locations = locations;
//    }

}
