package co.uk.app.payloads;

import java.util.Date;

/**
 * Created by ${Eclair} on 8/27/2018.
 */
public class JWTAuthenticationResponseDTO {

    private String accessToken;
    private String tokenType = "Bearer";
    private Date expiredDate;

    public JWTAuthenticationResponseDTO(final String accessToken, final Date expiredDate) {
        this.accessToken = accessToken;
        this.expiredDate = expiredDate;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public Date getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Date expiredDate) {
        this.expiredDate = expiredDate;
    }
}
