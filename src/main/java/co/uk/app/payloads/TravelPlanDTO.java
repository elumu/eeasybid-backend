package co.uk.app.payloads;

import co.uk.app.account.dtos.CreateAccountDTO;

import java.util.List;

/**
 * Created by ${Eclair} on 12/31/2018.
 */
public class TravelPlanDTO {

    private CreateAccountDTO createAccountDTO;
    private List<DestinationDTO> destinations;

    public List<DestinationDTO> getDestinations() {
        return destinations;
    }

    public void setDestinations(List<DestinationDTO> destinations) {
        this.destinations = destinations;
    }

    public CreateAccountDTO getCreateAccountDTO() {
        return createAccountDTO;
    }

    public void setCreateAccountDTO(CreateAccountDTO createAccountDTO) {
        this.createAccountDTO = createAccountDTO;
    }
}
