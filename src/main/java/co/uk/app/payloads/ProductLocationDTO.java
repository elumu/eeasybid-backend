package co.uk.app.payloads;

/**
 * Created by ${Eclair} on 12/1/2018.
 */
public class ProductLocationDTO {
    private String longitude;
    private String latitude;

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

}
