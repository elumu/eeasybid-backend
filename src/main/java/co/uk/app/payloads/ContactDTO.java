package co.uk.app.payloads;

/**
 * Created by ${Eclair} on 12/2/2018.
 */
public class ContactDTO {
    private String contact;

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}
