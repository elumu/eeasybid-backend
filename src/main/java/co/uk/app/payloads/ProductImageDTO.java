package co.uk.app.payloads;

/**
 * Created by ${Eclair} on 12/1/2018.
 */
public class ProductImageDTO {
    private String id;
    private String path;
    private ProductDTO product;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public ProductDTO getProduct() {
        return product;
    }

    public void setProduct(ProductDTO product) {
        this.product = product;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
