package co.uk.app.payloads;

/**
 * Created by ${Eclair} on 4/20/2019.
 */
public class ProductRatingDTO {
    public Integer rating;
    public ProductReviewDTO productReview;

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public ProductReviewDTO getProductReview() {
        return productReview;
    }

    public void setProductReview(ProductReviewDTO productReview) {
        this.productReview = productReview;
    }
}
