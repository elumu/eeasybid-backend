package co.uk.app.payloads;

/**
 * Created by ${Eclair} on 12/1/2018.
 */
public class ProductDescriptionDTO {
    private String price;
    private String offerContent;
    private String descriptionOfOffer;

    public void setPrice(String price) {
        this.price = price;
    }

    public void setOfferContent(String offerContent) {
        this.offerContent = offerContent;
    }

    public void setDescriptionOfOffer(String descriptionOfOffer) {
        this.descriptionOfOffer = descriptionOfOffer;
    }

    public String getPrice() {
        return price;
    }

    public String getOfferContent() {
        return offerContent;
    }

    public String getDescriptionOfOffer() {
        return descriptionOfOffer;
    }
}
