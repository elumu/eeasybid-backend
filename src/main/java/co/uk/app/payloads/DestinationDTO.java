package co.uk.app.payloads;

/**
 * Created by ${Eclair} on 12/31/2018.
 */
public class DestinationDTO {
    private String region;
    private String country;
    private String city;
    private RangeDateDTO rangeDateDTO;

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public RangeDateDTO getRangeDateDTO() {
        return rangeDateDTO;
    }

    public void setRangeDateDTO(RangeDateDTO rangeDateDTO) {
        this.rangeDateDTO = rangeDateDTO;
    }
}
