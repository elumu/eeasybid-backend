package co.uk.app.payloads;

/**
 * Created by ${Eclair} on 11/30/2018.
 */
public class CountDownDTO {

    private String startingDate;
    private String endingDate;

    public String getStartingDate() {
        return startingDate;
    }

    public void setStartingDate(String startingDate) {
        this.startingDate = startingDate;
    }

    public String getEndingDate() {
        return endingDate;
    }

    public void setEndingDate(String endingDate) {
        this.endingDate = endingDate;
    }
}
