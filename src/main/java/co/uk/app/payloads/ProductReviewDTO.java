package co.uk.app.payloads;

/**
 * Created by ${Eclair} on 4/20/2019.
 */
public class ProductReviewDTO {
    private String reviewer;
    private ProductRatingDTO productRatingDTO;
    private String contents;
    private ProductDTO productDTO;

    public String getReviewer() {
        return reviewer;
    }

    public void setReviewer(String reviewer) {
        this.reviewer = reviewer;
    }

    public ProductRatingDTO getProductRatingDTO() {
        return productRatingDTO;
    }

    public void setProductRatingDTO(ProductRatingDTO productRatingDTO) {
        this.productRatingDTO = productRatingDTO;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public ProductDTO getProductDTO() {
        return productDTO;
    }

    public void setProductDTO(ProductDTO productDTO) {
        this.productDTO = productDTO;
    }
}
