package co.uk.app.payloads;

/**
 * Created by ${Eclair} on 12/1/2018.
 */
public class ProductDetailDTO {
    private ProductDTO productDTO;
    private ProductDescriptionDTO productDescriptionDTO;
    private ProductInfoDTO productInfoDTO;
    private ProductLocationDTO productLocationDTO;

    public void setProductDTO(ProductDTO productDTO) {
        this.productDTO = productDTO;
    }

    public void setProductDescriptionDTO(ProductDescriptionDTO productDescriptionDTO) {
        this.productDescriptionDTO = productDescriptionDTO;
    }

    public void setProductInfoDTO(ProductInfoDTO productInfoDTO) {
        this.productInfoDTO = productInfoDTO;
    }

    public void setProductLocationDTO(ProductLocationDTO productLocationDTO) {
        this.productLocationDTO = productLocationDTO;
    }

    public ProductDTO getProductDTO() {
        return productDTO;
    }

    public ProductDescriptionDTO getProductDescriptionDTO() {
        return productDescriptionDTO;
    }

    public ProductInfoDTO getProductInfoDTO() {
        return productInfoDTO;
    }

    public ProductLocationDTO getProductLocationDTO() {
        return productLocationDTO;
    }
}
