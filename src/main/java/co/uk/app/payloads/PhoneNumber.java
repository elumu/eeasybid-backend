package co.uk.app.payloads;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

/**
 * Created by ${Eclair} on 7/20/2019.
 */
@Getter
@Setter
public class PhoneNumber {
    @NotBlank
    private String phoneNumber;
}
