package co.uk.app.payloads;

import javax.validation.constraints.NotBlank;

/**
 * Created by ${Eclair} on 5/19/2019.
 */
public class PersonalDetailsDTO {
    @NotBlank
    private String dateOfBirth;
    @NotBlank
    private String gender;
    @NotBlank
    private String phoneNumber;

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}
