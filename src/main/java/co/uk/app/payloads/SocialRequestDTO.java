package co.uk.app.payloads;

import javax.validation.constraints.NotBlank;

/**
 * Created by ${Eclair} on 6/13/2019.
 */
public class SocialRequestDTO {
    @NotBlank
    private String idToken;

    public String getIdToken() {
        return idToken;
    }

    public void setIdToken(String idToken) {
        this.idToken = idToken;
    }
}
