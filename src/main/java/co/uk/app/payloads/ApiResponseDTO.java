package co.uk.app.payloads;

/**
 * Created by ${Eclair} on 8/27/2018.
 */
public class ApiResponseDTO {

    private int status;
    private Boolean success;
    private String message;
    private Object data;

    public ApiResponseDTO(final Object data){
        this.data = data;
    }

    public ApiResponseDTO(final int status, final Boolean success, final String message) {
        this.status = status;
        this.success = success;
        this.message = message;
    }

    public ApiResponseDTO(final int status, final Boolean success, final String message, final Object data) {
        this(status, success, message);
        this.data = data;
    }

    public int getStatus() {
        return status;
    }
    public void setStatus(int status) {
        this.status = status;
    }
    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(final Boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(final String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
