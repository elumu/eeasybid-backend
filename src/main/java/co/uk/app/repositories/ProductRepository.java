package co.uk.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import co.uk.app.entities.product.Product;

/**
 * Created by ${Eclair} on 11/7/2018.
 */
public interface ProductRepository extends JpaRepository<Product, Long> {

}
