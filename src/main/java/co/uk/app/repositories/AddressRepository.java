package co.uk.app.repositories;

import co.uk.app.entities.common.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by ${Eclair} on 5/19/2019.
 */
@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {

}
