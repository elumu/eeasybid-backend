package co.uk.app.repositories;

import co.uk.app.entities.product.Price;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by ${Eclair} on 4/22/2019.
 */
public interface PriceRepository extends JpaRepository<Price, Long> {
    @Override
    Optional<Price> findById(Long aLong);
}
