package co.uk.app.repositories;

import co.uk.app.account.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by ${Eclair} on 8/26/2018.
 */
@Repository("roleRepository")
public interface RolesRepository extends JpaRepository<Role, Integer> {
    @Override
    Optional<Role> findById(Integer integer);
}
