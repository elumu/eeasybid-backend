package co.uk.app.repositories;

import co.uk.app.account.entities.ActivateAccountToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by ${Eclair} on 4/6/2019.
 */
@Repository("activateAccountTokenRepository")
public interface ActivateAccountTokenRepository extends JpaRepository<ActivateAccountToken, Long> {
    @Override
    Optional<ActivateAccountToken> findById(Long aLong);
    Optional<ActivateAccountToken> findByToken(String token);
}
