package co.uk.app.repositories;

import co.uk.app.account.entities.UserAccountProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by ${Eclair} on 4/6/2019.
 */
@Repository("userProfileRepository")
public interface UserProfileRepository extends JpaRepository<UserAccountProfile, Long> {
    @Override
    Optional<UserAccountProfile> findById(Long aLong);
}
