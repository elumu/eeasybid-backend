package co.uk.app.repositories;

import co.uk.app.entities.product.Vendor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VendorRepository extends JpaRepository<Vendor, Long>
{
	List<Vendor> findByProducts_id(long id);
}
