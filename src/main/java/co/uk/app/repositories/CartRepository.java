package co.uk.app.repositories;

import co.uk.app.entities.commerce.Cart;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by ${Eclair} on 5/28/2019.
 */
@Repository
public interface CartRepository extends JpaRepository<Cart, Long> {
}
