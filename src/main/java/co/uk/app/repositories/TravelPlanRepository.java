package co.uk.app.repositories;

import co.uk.app.entities.travel.TravelPlan;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by ${Eclair} on 12/31/2018.
 */
public interface TravelPlanRepository extends JpaRepository<TravelPlan, Long> {
}
