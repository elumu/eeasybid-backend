package co.uk.app.repositories;

import co.uk.app.entities.common.File;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by ${Eclair} on 5/20/2019.
 */
public interface FileRepository extends JpaRepository<File, String> {
}
