package co.uk.app.repositories;

import co.uk.app.entities.product.ProductImage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Created by ${Eclair} on 4/20/2019.
 */
public interface ProductImageRepository extends JpaRepository<ProductImage, Long> {
    @Override
    Optional<ProductImage> findById(Long aLong);

    List<ProductImage> findByProductId(String productId);
}
