package co.uk.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import co.uk.app.entities.product.ProductReview;


public interface ProductReviewRepository extends JpaRepository<ProductReview, Long>
{
}
