package co.uk.app.security.oauth2.account;

import java.util.Map;

/**
 * Created by ${Eclair} on 6/10/2019.
 */
public class GoogleOAuth2AccountInfo extends OAuth2AccountInfo {

    public GoogleOAuth2AccountInfo(Map<String, Object> attributes) {
        super(attributes);
    }

    @Override
    public String getId() {
        return (String) attributes.get("sub");
    }

    @Override
    public String getName() {
        return (String) attributes.get("name");
    }

    @Override
    public String getEmail() {
        return (String) attributes.get("email");
    }

    @Override
    public String getImageUrl() {
        return (String) attributes.get("picture");
    }
}
