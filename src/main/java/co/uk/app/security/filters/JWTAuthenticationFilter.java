package co.uk.app.security.filters;

import co.uk.app.account.JWTTokenProviderService;
import co.uk.app.services.impl.JWTTokenProviderServiceImpl;
import co.uk.app.services.impl.UserDetailsServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Created by ${Eclair} on 8/27/2018.
 */
@Component
public class JWTAuthenticationFilter extends OncePerRequestFilter
{

	private static Logger logger = LoggerFactory.getLogger(JWTAuthenticationFilter.class);

	private final JWTTokenProviderService tokenProvider;
	private UserDetailsServiceImpl userDetailsService;

	/**
	 * Instantiates a new Jwt authentication filter.
	 *
	 * @param tokenProvider
	 * 		the token provider
	 * @param userDetailsService
	 * 		the user details service
	 */
	@Autowired
	public JWTAuthenticationFilter(final JWTTokenProviderServiceImpl tokenProvider,
			final UserDetailsServiceImpl userDetailsService)
	{
		this.tokenProvider = tokenProvider;
		this.userDetailsService = userDetailsService;
	}

	@Override
	protected void doFilterInternal(final HttpServletRequest req, final HttpServletResponse res, final FilterChain filterChain)
			throws ServletException, IOException
	{
		try
		{
			final String jwt = getJwtFromRequest(req);
			if (StringUtils.hasText(jwt) && tokenProvider.validateToken(jwt))
			{
				final Long userId = tokenProvider.getUserIdFromJWT(jwt);

				final UserDetails userDetails = userDetailsService.loadByUserId(userId);
				final UsernamePasswordAuthenticationToken authentication =
						new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
				authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(req));

				SecurityContextHolder.getContext().setAuthentication(authentication);
			}
		}
		catch (final Exception ex)
		{
			logger.error("Could not set user authentication in security context", ex);
		}
		filterChain.doFilter(req, res);
	}

	/**
	 * get authorization header and reads the token and return it
	 */
	private String getJwtFromRequest(final HttpServletRequest req)
	{
		final String bearerToken = req.getHeader("Authorization");
		if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer "))
		{
			return bearerToken.substring(7);
		}
		return null;
	}
}
