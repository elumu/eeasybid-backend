package co.uk.app.security;

import co.uk.app.account.entities.Account;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.oauth2.core.user.OAuth2User;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Created by ${Eclair} on 8/21/2018.
 */
public class Principal extends Account implements UserDetails, OAuth2User
{
	private Collection<? extends GrantedAuthority> authorities;
	private Map<String, Object> attributes;

	public Principal(final Account account, final Collection<? extends GrantedAuthority> authorities)
	{
		//super(account);
		this.authorities = authorities;
	}

	public static Principal create(final Account account)
	{
		final List<GrantedAuthority> authorities = account.getRoles().stream().map(role ->
				new SimpleGrantedAuthority(role.getRole())
		).collect(Collectors.toList());

		return new Principal(
                account, authorities
		);
	}

	public static Principal create(final Account account, final Map<String, Object> attributes) {
		final Principal userPrincipal = Principal.create(account);
		userPrincipal.setAttributes(attributes);
		return userPrincipal;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities()
	{
		return authorities;
	}

	@Override
	public Map<String, Object> getAttributes() {
		return null;
	}

	@Override
	public String getPassword()
	{
		return super.getPassword();
	}

	@Override
	public String getUsername()
	{
		return super.getEmail();
	}

	@Override
	public boolean isAccountNonExpired()
	{
		return true;
	}

	@Override
	public boolean isAccountNonLocked()
	{
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired()
	{
		return true;
	}

	@Override
	public boolean isEnabled()
	{
		return true;
	}

	@Override
	public String getName() {
		return null;
	}

	public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
		this.authorities = authorities;
	}

	public void setAttributes(Map<String, Object> attributes) {
		this.attributes = attributes;
	}
}
