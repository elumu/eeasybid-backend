package co.uk.app.facades;

import co.uk.app.account.entities.ActivateAccountToken;
import co.uk.app.account.entities.PasswordResetToken;
import co.uk.app.account.entities.Account;
import co.uk.app.payloads.LoginRequestDTO;
import co.uk.app.payloads.ResetPasswordRequestDTO;
import co.uk.app.payloads.SignUpRequestDTO;
import org.springframework.security.core.Authentication;

import java.util.Optional;


/**
 * Created by ${Eclair} on 8/26/2018.
 */
public interface AuthFacade
{
    /**
     * Send email.
     * the token
     *
     * @param subject     the subject
     * @param content     the content
     * @param account the user account
     */
    void sendEmail(final String subject, final String content, final Account account);

    /**
     * Activate account.
     *
     * @param account the user account
     */
    void activateAccount(final Account account);

    /**
     * Gets user account by email.
     *
     * @param email the email
     * @return the user account by email
     */
    Optional<Account> getUserAccountByEmail(String email);

    /**
     * Gets user account by email.
     *
     * @param id the id
     * @return the user account by email
     */
    Optional<Account> getUserAccountById(final Long id);

    /**
     * Check if account exist boolean.
     *
     * @param email the email
     * @return the boolean
     */
    boolean emailExist(final String email);

    /**
     * Create account user account.
     *
     * @param signUpRequestDTO the sign up request dto
     * @return the user account
     */
    Account createAccount(final SignUpRequestDTO signUpRequestDTO);

    /**
     * Authenticate account authentication.
     *
     * @param loginRequestDTO the login request dto
     * @return the authentication
     */
    Authentication authenticate(LoginRequestDTO loginRequestDTO);

    /**
     * Gets password reset token.
     *
     * @param token the token
     * @return the password reset token
     */
    Optional<PasswordResetToken> getPasswordResetToken(String token);

    /**
     * Find activate account token optional.
     *
     * @param activateToken the activate token
     * @return the optional
     */
    Optional<ActivateAccountToken>findActivateAccountToken(String activateToken);

    /**
     * Create password reset token for user password reset token.
     *
     * @param account the user account
     * @param token       the token
     * @return the password reset token
     */
    PasswordResetToken createPasswordResetTokenForUser(final Account account, final String token);

    /**
     * Reset account password user account.
     *
     * @param passwordResetToken the password reset token
     * @param resetPassword      the reset password
     * @return the user account
     */
    Account resetAccountPassword(PasswordResetToken passwordResetToken, ResetPasswordRequestDTO resetPassword);
}
