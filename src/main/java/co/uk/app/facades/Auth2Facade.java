package co.uk.app.facades;

import co.uk.app.account.entities.Account;
import co.uk.app.payloads.SocialSignUpRequestDTO;

import java.util.Optional;

/**
 * Created by ${Eclair} on 6/13/2019.
 */
public interface Auth2Facade {
    /**
     * Gets user account by email.
     *
     * @param email the email
     * @return the user account by email
     */
    Optional<Account> getUserAccountByEmail(String email);


    /**
     * Create account user account.
     *
     * @param SocialSignUpRequestDTO the social sign up request dto
     * @return the user account
     */
    Account createAccount(final SocialSignUpRequestDTO SocialSignUpRequestDTO);
}
