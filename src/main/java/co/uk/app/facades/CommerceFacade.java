package co.uk.app.facades;

import co.uk.app.entities.commerce.Cart;

import java.util.List;
import java.util.Optional;

/**
 * Created by ${Eclair} on 5/28/2019.
 */
public interface CommerceFacade {
    Cart createShoppingCart(Long userId, List<String> listOfProductId);

    Optional<Cart> findCartById(Long cartId);

    float computerOrderPrice(Cart shoppingCart);
}
