package co.uk.app.facades;

import co.uk.app.account.entities.Account;
import co.uk.app.account.entities.UserAccountProfile;
import co.uk.app.entities.common.Address;
import co.uk.app.payloads.AddressDTO;

import java.util.List;
import java.util.Optional;

/**
 * Created by ${Eclair} on 5/19/2019.
 */
public interface AccountFacade {
    /**
     * Fin all accounts list.
     *
     * @return the list
     */
    List<Account> findAllAccounts();

    /**
     * Find account by id user account.
     *
     * @param id the id
     * @return the user account
     */
    Optional<Account> findAccountById(Long id);

    /**
     * Gets account profile.
     *
     * @param accessToken the access token
     * @return the account profile
     */
    Optional<UserAccountProfile> getAccountProfile(String accessToken);

    /**
     * Create address.
     *
     * @param addressDTO the address dto
     * @return the address
     */
    Address createAddress(AddressDTO addressDTO);

    /**
     * Find account by email.
     *
     * @param email the email
     * @return the optional
     */
    Optional<Account> findAccountByEmail(String email);
}
