package co.uk.app.facades.impl;

import co.uk.app.entities.commerce.Cart;
import co.uk.app.facades.CommerceFacade;
import co.uk.app.services.CommerceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * Created by ${Eclair} on 5/28/2019.
 */
@Component
public class CommerceFacadeImpl implements CommerceFacade {
    private CommerceService commerceService;

    @Autowired
    public CommerceFacadeImpl(final CommerceService commerceService) {
        this.commerceService = commerceService;
    }

    @Override
    public Cart createShoppingCart(Long userId, List<String> listOfProductId) {
        return commerceService.createShoppingCart(userId, listOfProductId);
    }

    @Override
    public Optional<Cart> findCartById(Long cartId) {
        return commerceService.findCartById(cartId);
    }

    @Override
    public float computerOrderPrice(Cart shoppingCart) {
        return commerceService.computeOrderPrice(shoppingCart);
    }
}
