package co.uk.app.facades.impl;

import co.uk.app.account.entities.Account;
import co.uk.app.account.entities.UserAccountProfile;
import co.uk.app.entities.common.Address;
import co.uk.app.facades.AccountFacade;
import co.uk.app.payloads.AddressDTO;
import co.uk.app.account.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

/**
 * Created by ${Eclair} on 5/19/2019.
 */
@Component
public class AccountFacadeImpl implements AccountFacade {
    private AccountService accountService;

    @Autowired
    public AccountFacadeImpl(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public List<Account> findAllAccounts()
    {
        return accountService.findAllAccounts();
    }

    @Override
    public Optional<Account> findAccountById(Long id) {
        return accountService.findAccountById(id);
    }
    @Override
    public Optional<UserAccountProfile> getAccountProfile(final String accessToken)
    {
        return null;
       // return accountService.findAccountForToken(accessToken);
    }

    @Override
    public Address createAddress(AddressDTO addressDTO) {
        return null;
        //return accountService.createAddress(addressDTO);
    }

    @Override
    public Optional<Account> findAccountByEmail(String email) {
        return accountService.findAccountByEmail(email);
    }
}
