package co.uk.app.facades.impl;

import co.uk.app.account.entities.ActivateAccountToken;
import co.uk.app.account.entities.PasswordResetToken;
import co.uk.app.account.entities.Account;
import co.uk.app.facades.AuthFacade;
import co.uk.app.payloads.LoginRequestDTO;
import co.uk.app.payloads.ResetPasswordRequestDTO;
import co.uk.app.payloads.SignUpRequestDTO;
import co.uk.app.account.AuthService;
import co.uk.app.services.impl.EmailServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Optional;


/**
 * Created by ${Eclair} on 8/27/2018.
 */
@Component
public class AuthFacadeImpl implements AuthFacade
{
	private static final Logger LOGGER = LoggerFactory.getLogger(AuthFacadeImpl.class);
	private final EmailServiceImpl emailService;
	private final AuthService authService;

	@Autowired
	public AuthFacadeImpl(
			final EmailServiceImpl emailService,
			final AuthService authService)
	{
		this.emailService = emailService;
		this.authService = authService;
	}


	@Override
	public void sendEmail(final String subject, final String content, final Account account)
	{
		LOGGER.info("Sending " + subject + " email to" + account.getEmail());
		emailService.sendMessage(subject, content, account);
	}

	@Override
	public void activateAccount(final Account account)
	{
		//account.setIsActive(true);
		authService.activateAccount(account);
		authService.deleteActivateAccountToken(account);
	}

	@Override
	public Optional<Account> getUserAccountByEmail(final String email)
	{
		return authService.findAccountByEmail(email);
	}

	@Override
	public Optional<Account> getUserAccountById(final Long id)
	{
		return authService.findAccountById(id);
	}

	@Override
	public boolean emailExist(final String email)
	{
		return authService.emailExist(email);
	}

	@Override
	public Account createAccount(final SignUpRequestDTO signUpRequestDTO)
	{
		return authService.createAccount(signUpRequestDTO);
	}

	@Override
	public Authentication authenticate(final LoginRequestDTO loginRequestDTO)
	{
		return authService.authenticate(loginRequestDTO);
	}

	@Override
	public Optional<PasswordResetToken> getPasswordResetToken(final String token)
	{
		return authService.findPasswordResetToken(token);
	}

	@Override
	public Optional<ActivateAccountToken> findActivateAccountToken(final String activateToken)
	{
		return authService.findActivateAccountToken(activateToken);
	}

	@Override
	public PasswordResetToken createPasswordResetTokenForUser(final Account account, final String token)
	{
		return authService.createPasswordResetTokenForUser(account, token);
	}

	@Override
	public Account resetAccountPassword(final PasswordResetToken passwordResetToken,
                                        final ResetPasswordRequestDTO resetPassword)
	{
		return authService.resetAccountPassword(passwordResetToken, resetPassword);
	}
}
