package co.uk.app.facades.impl;

import co.uk.app.account.entities.Account;
import co.uk.app.facades.Auth2Facade;
import co.uk.app.payloads.SocialSignUpRequestDTO;
import co.uk.app.account.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * Created by ${Eclair} on 6/13/2019.
 */
@Component
public class Auth2FacadeImpl implements Auth2Facade {
    private AuthService authService;

    @Autowired
    public Auth2FacadeImpl(AuthService authService) {
        this.authService = authService;
    }

    @Override
    public Optional<Account> getUserAccountByEmail(String email) {
        return Optional.empty();
    }

    @Override
    public Account createAccount(SocialSignUpRequestDTO SocialSignUpRequestDTO) {
        return authService.createAccount(SocialSignUpRequestDTO);
    }

}
